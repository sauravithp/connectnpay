package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "service_payment_parameter")
@Table(name = "SERVICE_PAYMENT_PARAMETER")
public class ServicePaymentParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "PARAM_NAME")
    private String paramName;
    @Basic(optional = false)
    @Column(name = "PARAM_CODE")
    private String paramCode;
    @Column(name = "IS_DEFAULT", length = 2)
    private Character isDefault;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "payment_code_type")
    @Basic(optional = false)
    @JoinColumn(name = "PAYMENT_CODE_TYPE_ID", referencedColumnName = "ID")
    @ManyToOne
    private PaymentCodeType paymentCodeType;
    @JoinColumn(name = "SERVICE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Service service;
    @Column(name = "PAYMENT_CODE_LENGTH")
    private Integer paymentCodeLength;
    @Column(name = "ACTIVE", length = 2)
    private Character active;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;

    public ServicePaymentParameter() {
    }

    public ServicePaymentParameter(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public PaymentCodeType getPaymentCodeType() {
        return paymentCodeType;
    }

    public void setPaymentCodeType(PaymentCodeType paymentCodeType) {
        this.paymentCodeType = paymentCodeType;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getPaymentCodeLength() {
        return paymentCodeLength;
    }

    public void setPaymentCodeLength(Integer paymentCodeLength) {
        this.paymentCodeLength = paymentCodeLength;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public Character getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Character isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ServicePaymentParameter[id=" + id + "]";
    }
}
