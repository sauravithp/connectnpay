package com.f1soft.connectnpay.topup.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author santosh
 */
public class StringUtility {

    public static boolean isEmptyString(String str) {
        if (str != null && str.trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static String stringValue(Object str) {
        if (str != null && str.toString().trim().length() > 0) {
            return str.toString();
        } else {
            return "";
        }
    }

    public static List arrayList(Object str) {
        if (str != null && ((List) str).size() > 0) {
            return (List) str;
        } else {
            return new ArrayList();
        }
    }

    public static String failureMessage(Object str) {
        if (str != null && str.toString().trim().length() > 0) {
            return str.toString();
        } else {
            return "Failed Retriving data";
        }
    }
}
