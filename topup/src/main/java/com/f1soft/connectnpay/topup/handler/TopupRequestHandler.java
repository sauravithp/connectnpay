package com.f1soft.connectnpay.topup.handler;

import com.f1soft.connectnpay.topup.dto.*;
import com.f1soft.connectnpay.topup.util.Formater;
import com.f1soft.connectnpay.topup.util.StringUtility;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.MultivaluedMap;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
public class TopupRequestHandler {
    public RequestDTO convertToRequestDTO(MultivaluedMap<String, String> requestMap) {
        String servicecode = requestMap.getFirst("servicecode");
        String requestid = requestMap.getFirst("requestid");
        String agenttransid = requestMap.getFirst("agenttransid");
        String mobileno = requestMap.getFirst("mobileno");
        String walletid = requestMap.getFirst("walletid");
        double amount = 0;

        try {
            amount = Double.parseDouble(requestMap.getFirst("amount"));
        } catch (Exception e) {
            log.error("Exception ", e);
        }

        String clientusername = requestMap.getFirst("clientusername");
        String password = requestMap.getFirst("password");
        String channel = requestMap.getFirst("channel");
        String customerid = requestMap.getFirst("customerid");
        String type = requestMap.getFirst("type");

        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setChannel(channel);
        requestDTO.setClientTraceId(agenttransid);
        requestDTO.setCustomerRefId(customerid);
        requestDTO.setRequestId(requestid);
        requestDTO.setRequestIdRef(requestid);
        requestDTO.setClientUsername(clientusername);
        requestDTO.setPassword(password);
        requestDTO.setAmount(new BigDecimal(amount));
        requestDTO.setServiceCode(servicecode);
        requestDTO.setServiceAttribute(mobileno);
        requestDTO.setWalletId(walletid);
        requestDTO.setType(type);
        requestDTO.setRequestLodgeDate(new Date());
        return requestDTO;
    }

    public RequestDTO convertToRequestDTO(BillPayRequest billPayRequest) {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setChannel(billPayRequest.getChannel());
        requestDTO.setClientTraceId(billPayRequest.getAgenttransid());
        requestDTO.setCustomerRefId(billPayRequest.getCustomerid());
        requestDTO.setRequestId(billPayRequest.getRequestid());
        requestDTO.setRequestIdRef(billPayRequest.getRequestid());
        requestDTO.setClientUsername(billPayRequest.getClientusername());
        requestDTO.setPassword(billPayRequest.getPassword());
        requestDTO.setAmount(billPayRequest.getAmount());
        requestDTO.setServiceCode(billPayRequest.getServicecode());
        requestDTO.setServiceAttribute(billPayRequest.getMobileno());
        requestDTO.setWalletId(billPayRequest.getWalletid());
        requestDTO.setType(billPayRequest.getType());
        requestDTO.setRequestLodgeDate(new Date());
        return requestDTO;
    }

    public boolean isParameterValid(RequestDTO requestDTO) {
        if (StringUtility.isEmptyString(requestDTO.getServiceCode())) {
            return false;
        }
        if (StringUtility.isEmptyString(requestDTO.getRequestId())) {
            return false;
        }
        if (StringUtility.isEmptyString(requestDTO.getClientTraceId())) {
            return false;
        }
        if (StringUtility.isEmptyString(requestDTO.getServiceAttribute())) {
            return false;
        }
        if (requestDTO.getAmount().equals(new BigDecimal(0))) {
            return false;
        }
        if (StringUtility.isEmptyString(requestDTO.getClientUsername())) {
            return false;
        }
        if (StringUtility.isEmptyString(requestDTO.getPassword())) {
            return false;
        }
        return true;
    }

    public RequestDTO convertServiceValidationParameters(final MultivaluedMap<String, String> requestMap) {
        String servicecode = requestMap.getFirst("servicecode");

        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setServiceCode(servicecode);
        return requestDTO;
    }

    public boolean isServiceValidationParameterValid(RequestDTO requestDTO) {
        return !StringUtility.isEmptyString(requestDTO.getServiceCode());
    }

    public boolean isBalanceLoadParameterValid(LoadClientBalanceRequest loadBalanceRequest) {
        if (loadBalanceRequest.getClientAccountId() == null) {
            return false;
        }
        if (loadBalanceRequest.getInitiatorId() == null) {
            return false;
        }
        if (loadBalanceRequest.getAmount().equals(new Double(0))) {
            return false;
        }
        if (StringUtility.isEmptyString(loadBalanceRequest.getRemarks())) {
            return false;
        }
        return true;
    }

    public RequestDTO convertToRequestDTO(LoadClientBalanceRequest loadClientBalanceRequest) {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setClientAccountId(loadClientBalanceRequest.getClientAccountId());
        requestDTO.setInitiatorId(loadClientBalanceRequest.getInitiatorId());
        requestDTO.setAmount(Formater.doubleToBigDecimal(loadClientBalanceRequest.getAmount()));
        requestDTO.setRemarks(loadClientBalanceRequest.getRemarks());
        return requestDTO;
    }

    public RequestDTO convertReversalParameters(final MultivaluedMap<String, String> requestMap) {
        Long clientRequestId = Long.parseLong(requestMap.getFirst("clientRequestId"));
        Long initiatorId = Long.parseLong(requestMap.getFirst("initiatorId"));
        String remarks = requestMap.getFirst("remarks");

        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setClientRequestId(clientRequestId);
        requestDTO.setInitiatorId(initiatorId);
        requestDTO.setRemarks(remarks);
        return requestDTO;
    }

    public ChangeStatusParameter convertToChangeStatusParameter(final MultivaluedMap<String, String> requestMap) {
        Long clientRequestId = Long.parseLong(requestMap.getFirst("clientRequestId"));
        Long initiatorId = Long.parseLong(requestMap.getFirst("initiatorId"));
        Integer status = Integer.parseInt(requestMap.getFirst("status"));
        String vendorTraceId = requestMap.getFirst("vendorTraceId");
        String vendorResponseCode = requestMap.getFirst("vendorResponseCode");
        String vendorResponseDesc = requestMap.getFirst("vendorResponseDesc");

        ChangeStatusParameter changeStatusParameter = new ChangeStatusParameter();
        changeStatusParameter.setClientRequestId(clientRequestId);
        changeStatusParameter.setInitiatorId(initiatorId);
        changeStatusParameter.setStatus(status);
        changeStatusParameter.setVendorTraceId(vendorTraceId);
        changeStatusParameter.setVendorResponseCode(vendorResponseCode);
        changeStatusParameter.setVendorResponseDesc(vendorResponseDesc);
        return changeStatusParameter;
    }

    public TransactionStatusDTO convertTransactionStatusParameters(final MultivaluedMap<String, String> requestMap) {
        String requestId = requestMap.getFirst("requestid");
        Double amount = Double.parseDouble(requestMap.getFirst("amount"));
        String username = requestMap.getFirst("clientusername");
        String password = requestMap.getFirst("password");

        TransactionStatusDTO statusRequestDTO = new TransactionStatusDTO();
        statusRequestDTO.setClientusername(username);
        statusRequestDTO.setPassword(password);

        List<CheckStatusTransaction> txnList = new ArrayList<>();

        CheckStatusTransaction transaction = new CheckStatusTransaction();
        transaction.setAmount(amount);
        transaction.setRequestId(requestId);

        txnList.add(transaction);

        statusRequestDTO.setTransactions(txnList);

        return statusRequestDTO;
    }

    public TransactionStatusDTO convertStatusRequestToStatusRequestDTO(TransactionCheckStatusRequest statusRequest) {
        String username = statusRequest.getClientusername();
        String password = statusRequest.getPassword();

        List<CheckStatusTransaction> txnList = new ArrayList<>();

        TransactionStatusDTO statusRequestDTO = new TransactionStatusDTO();
        statusRequestDTO.setClientusername(username);
        statusRequestDTO.setPassword(password);

        for (CheckStatusTxnList txn : statusRequest.getTransactions()) {
            CheckStatusTransaction transaction = new CheckStatusTransaction();
            transaction.setAmount(txn.getAmount());
            transaction.setRequestId(txn.getRequestId());
            txnList.add(transaction);
        }

        statusRequestDTO.setTransactions(txnList);

        return statusRequestDTO;
    }

    public RequestDTO convertServiceStatusParameters(ServiceStatusRequest request) {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setClientUsername(request.getClientusername());
        requestDTO.setPassword(request.getPassword());
        requestDTO.setServiceCode(request.getServiceCode());
        return requestDTO;
    }
}
