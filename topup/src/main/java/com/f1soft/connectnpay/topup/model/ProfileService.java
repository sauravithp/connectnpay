package com.f1soft.connectnpay.topup.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author yogesh
 */
@Getter
@Setter
@Entity()
@Cacheable(true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "profile_service")
@Table(name = "PROFILE_SERVICE")
public class ProfileService implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service")
    @JoinColumn(name = "SERVICE_ID", referencedColumnName = "ID")
    @ManyToOne()
    private Service service;
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Profile profile;
    @Basic(optional = false)
    @Column(name = "ACTIVE")
    private char active;
    @Basic(optional = false)
    @Column(name = "DO_SETTLEMENT", nullable = false, length = 1)
    private char doSettlement;
    @Basic(optional = false)
    @Column(name = "CALL_TXN_API", nullable = false, length = 1)
    private char callTxnApi;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "profile_service_scheme")
    @OneToMany(mappedBy = "profileService", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ProfileServiceScheme> profileServiceSchemes;

    public ProfileService() {
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ProfileService[id=" + id + "]";
    }
}
