package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author yogesh
 */
@Entity()
@Table(name = "SERVICE_GROUP")
public class ServiceGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME", nullable = false, length = 150)
    private String name;
    @Column(name = "DESCRIPTION", length = 250)
    private String description;

    public ServiceGroup() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
