package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 *
 * @author santosh
 */
@Getter
@Setter
public class ServiceTxnParamDTO extends ModelBase {

    private BigDecimal txnAmount;
    private BigDecimal vendorAmount;
    private String txnApiSettlement;

}
