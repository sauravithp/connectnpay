package com.f1soft.connectnpay.topup.util;

import com.f1soft.connectnpay.topup.configuration.RedisServerConfig;
import com.f1soft.connectnpay.topup.configuration.ServerConfig;
import com.f1soft.connectnpay.topup.constant.ApplicationConfigConstant;
import com.f1soft.connectnpay.topup.dao.FetchDataDAO;
import com.f1soft.connectnpay.topup.dto.ProviderResultCodeResponse;
import com.f1soft.connectnpay.topup.model.ApplicationConfig;
import com.f1soft.connectnpay.topup.model.MPOSDefaultParameter;
import com.f1soft.connectnpay.topup.model.ProviderResultCode;
import com.f1soft.connectnpay.topup.model.ServiceResponseInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

import static com.f1soft.connectnpay.topup.constant.Common.*;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Slf4j
@Component("resourceLoader")
@Scope("singleton")
@AllArgsConstructor
public class ResourceLoader {


    private final FetchDataDAO fetchDataDAO;

    private static Map<String, String> configMap;
    private static Map<String, ProviderResultCodeResponse> providerResultCodeMap;
    public static Map<String, ServiceResponseInfo> responseMap;
    private static Map<String, String> mposDefaultParameterMap;

    private static Map<String, ServiceResponseInfo> systemResponseCodeMap;
    public static List<String> superClients;

    private static ServerConfig serverConfig;
    private static RedisServerConfig redisServerConfig;
    public static String runningTime;
    private YamlReader reader = null;

    @PostConstruct
    public void bootstrap() {
        PropertyConfigurator.configure("log4j.properties");
        loadBanner();
        reader = new YamlReader();
        loadServerConfig();
        initializeConfiguration();
        initializeMPOSDefaultParameter();
        initializeServerResponse();
        initializeProviderResponse();

        runningTime = DateHelper.convertToString(new Date(), DateHelper.DATE_FORMAT_YYYYMMDD_TIME24);
    }

    private void loadBanner() {
        log.info(" ____  _____  ____  __  __  ____    ___  ____  ____  _  _  ____  ____ \n"
                + "(_  _)(  _  )(  _ \\(  )(  )(  _ \\  / __)( ___)(  _ \\( \\/ )( ___)(  _ \\\n"
                + "  )(   )(_)(  )___/ )(__)(  )___/  \\__ \\ )__)  )   / \\  /  )__)  )   /\n"
                + " (__) (_____)(__)  (______)(__)    (___/(____)(_)\\_)  \\/  (____)(_)\\_)\n"
                + "");
    }

    private void loadServerConfig() {
        serverConfig = reader.load("topup-server-config.yml", ServerConfig.class);
        redisServerConfig = reader.load("redis-server-config.yml", RedisServerConfig.class);

        log.info("Server config : {}", serverConfig);
        log.info("Redis Server config : {}", redisServerConfig);
    }

    private void initializeConfiguration() {
        List<ApplicationConfig> applicationConfigList = fetchDataDAO.getApplicationConfigList();
        configMap = new HashMap<>();
        for (ApplicationConfig applicationConfig : applicationConfigList) {
            configMap.put(applicationConfig.getConfigKey(), applicationConfig.getConfigValue());
        }
        loadSuperClients();
    }

    private void loadSuperClients() {
        superClients = new ArrayList<>();

        if (configMap.containsKey(ApplicationConfigConstant.SUPER_CLIENTS)) {
            String superClientConfig = configMap.get(ApplicationConfigConstant.SUPER_CLIENTS);
            if (superClientConfig != null && superClientConfig.trim().length() > 0) {
                String[] clientArr = superClientConfig.split(",");

                if (clientArr.length > 0) {
                    superClients.addAll(Arrays.asList(clientArr));
                }
            }
        }
        log.info("Super clients : " + superClients);
    }

    private void initializeMPOSDefaultParameter() {
        List<MPOSDefaultParameter> parameters = fetchDataDAO.getMPOSDefaultParameters();
        mposDefaultParameterMap = new HashMap<>();
        for (MPOSDefaultParameter ntcDefaultParameter : parameters) {
            mposDefaultParameterMap.put(ntcDefaultParameter.getConfigKey(), ntcDefaultParameter.getConfigValue());
        }
    }

    private void initializeServerResponse() {
        responseMap = new HashMap<>();
        systemResponseCodeMap = new HashMap<>();

        List<ServiceResponseInfo> serviceResponseInfos = fetchDataDAO.getServiceResponseInfoList();
        for (ServiceResponseInfo serviceResponseInfo : serviceResponseInfos) {
            if (serviceResponseInfo.getResponseType().equalsIgnoreCase("SYS")) {
                responseMap.put(serviceResponseInfo.getRefCode(), serviceResponseInfo);
                systemResponseCodeMap.put(serviceResponseInfo.getRefCode(), serviceResponseInfo);
            }
        }
    }

    private void initializeProviderResponse() {
        providerResultCodeMap = new HashMap<>();
        List<ProviderResultCode> providerResultCodes = fetchDataDAO.getProviderResultCodeList();

        for (ProviderResultCode providerResultCode : providerResultCodes) {

            String providerCode = providerResultCode.getProviderCode();

            if (providerResultCodeMap.containsKey(providerCode)) {

                ProviderResultCodeResponse providerResultCodeResponse = providerResultCodeMap.get(providerCode);

                if (providerResultCode.getResultType().equalsIgnoreCase(PROVIDER_SUCCESS_RESPONSE)) {

                    Map<String, String> successCodeMap = providerResultCodeResponse.getSuccessCodeMap();

                    successCodeMap.put(providerResultCode.getResultCode().toUpperCase(), providerResultCode.getResultDesc());

                    //set success
                    providerResultCodeResponse.setSuccessCodeMap(successCodeMap);

                } else if (providerResultCode.getResultType().equalsIgnoreCase(PROVIDER_TIMEOUT_RESPONSE)) {

                    Map<String, String> timeoutCodeMap = providerResultCodeResponse.getTimeoutCodeMap();

                    timeoutCodeMap.put(providerResultCode.getResultCode().toUpperCase(), providerResultCode.getResultDesc());

                    //set timeout
                    providerResultCodeResponse.setTimeoutCodeMap(timeoutCodeMap);

                } else if (providerResultCode.getResultType().equalsIgnoreCase(PROVIDER_FAILURE_RESPONSE)) {

                    Map<String, String> failureCodeMap = providerResultCodeResponse.getFailureCodeMap();

                    failureCodeMap.put(providerResultCode.getResultCode().toUpperCase(), providerResultCode.getResultDesc());

                    //set failure
                    providerResultCodeResponse.setFailureCodeMap(failureCodeMap);

                }

                providerResultCodeMap.put(providerCode, providerResultCodeResponse);

            } else {

                ProviderResultCodeResponse providerResultCodeResponse = new ProviderResultCodeResponse();
                providerResultCodeResponse.setSuccessCodeMap(new HashMap<>());
                providerResultCodeResponse.setTimeoutCodeMap(new HashMap<>());
                providerResultCodeResponse.setFailureCodeMap(new HashMap<>());

                if (providerResultCode.getResultType().equalsIgnoreCase(PROVIDER_SUCCESS_RESPONSE)) {

                    Map<String, String> successCodeMap = providerResultCodeResponse.getSuccessCodeMap();

                    successCodeMap.put(providerResultCode.getResultCode().toUpperCase(), providerResultCode.getResultDesc());

                    //set success
                    providerResultCodeResponse.setSuccessCodeMap(successCodeMap);

                } else if (providerResultCode.getResultType().equalsIgnoreCase(PROVIDER_TIMEOUT_RESPONSE)) {

                    Map<String, String> timeoutCodeMap = providerResultCodeResponse.getTimeoutCodeMap();

                    timeoutCodeMap.put(providerResultCode.getResultCode().toUpperCase(), providerResultCode.getResultDesc());

                    //set timeout
                    providerResultCodeResponse.setTimeoutCodeMap(timeoutCodeMap);

                } else if (providerResultCode.getResultType().equalsIgnoreCase(PROVIDER_FAILURE_RESPONSE)) {

                    Map<String, String> failureCodeMap = providerResultCodeResponse.getFailureCodeMap();

                    failureCodeMap.put(providerResultCode.getResultCode().toUpperCase(), providerResultCode.getResultDesc());

                    //set failure
                    providerResultCodeResponse.setFailureCodeMap(failureCodeMap);

                }

                providerResultCodeMap.put(providerCode, providerResultCodeResponse);
            }
        }
    }

    public static Map<String, String> getVendorSuccessCodes(String vendorCode) {
        return providerResultCodeMap.get(vendorCode).getSuccessCodeMap();
    }

    public static Map<String, String> getVendorTimeoutCodes(String vendorCode) {
        return providerResultCodeMap.get(vendorCode).getTimeoutCodeMap();
    }

    public static Map<String, String> getVendorFailureCodes(String vendorCode) {
        return providerResultCodeMap.get(vendorCode).getFailureCodeMap();
    }

    public static String findConfigValueByKey(String configKey) {
        return configMap.get(configKey);
    }

    public static Map<String, String> getMposDefaultParameterMap() {
        return mposDefaultParameterMap;
    }

    public static Map<String, ServiceResponseInfo> getSystemResponseCodeMap() {
        return systemResponseCodeMap;
    }

    public static ServerConfig getServerConfig() {
        return serverConfig;
    }

    public static RedisServerConfig getRedisServerConfig() {
        return redisServerConfig;
    }
}
