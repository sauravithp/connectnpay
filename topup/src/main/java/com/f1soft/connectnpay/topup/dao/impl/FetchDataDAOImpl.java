package com.f1soft.connectnpay.topup.dao.impl;

import com.f1soft.connectnpay.topup.constant.EhCacheConstants;
import com.f1soft.connectnpay.topup.dao.FetchDataDAO;
import com.f1soft.connectnpay.topup.model.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.f1soft.connectnpay.topup.constant.KeyConstant.*;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Slf4j
public class FetchDataDAOImpl implements FetchDataDAO {

    @PersistenceContext
    private EntityManager em;


    @Override
    public Client authenticateClient(String username) {
        Client client = new Client();
        try {
            Query query = em.createNamedQuery("Client.findByUsername");
            query.setParameter("username", username);
            query.setHint(EhCacheConstants.HintConstant.NAME, Boolean.TRUE);
            client = (Client) query.getSingleResult();
            client.setObtained(true);
            return client;
        } catch (NoResultException e) {
            return null;
        } catch (Exception e) {
            log.error("Exception ", e);
            return client;
        }
    }

//    @Cacheable(identifier = SERVICE + ALL, key = "#serviceId")
    @Override
    public Service getService(Long serviceId) {
        try {
            Query query = em.createNamedQuery("Service.findById");
            query.setParameter("id", serviceId);
            query.setHint(EhCacheConstants.HintConstant.NAME, Boolean.TRUE);
            Service service = (Service) query.getSingleResult();
            return service;
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientAccount getClientAccountByClientId(Long clientId) {
        try {
            Query query = em.createNamedQuery("ClientAccount.findByClientId");
            query.setParameter("clientId", clientId);
            return (ClientAccount) query.getSingleResult();
        } catch (NoResultException e) {
            log.error("No result for client id : " + clientId);
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientAccount getClientAccount(Long id) {
        try {
            Query query = em.createNamedQuery("ClientAccount.findById");
            query.setParameter("id", id);
            return (ClientAccount) query.getSingleResult();
        } catch (NoResultException e) {
            log.error("No result found for client account id : " + id);
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientRequest getClientRequestByRequestId(String requestId) {
        ClientRequest clientRequest = new ClientRequest();
        try {
            Query query = em.createNamedQuery("ClientRequest.findByRequestId");
            query.setParameter("requestId", requestId);
            query.setMaxResults(1);
            return (ClientRequest) query.getSingleResult();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
            clientRequest.setTxnErrorDesc("EXP");
            return clientRequest;
        }
        return null;
    }

    @Override
    public ClientRequest getClientRequestById(Long id) {
        try {
            Query query = em.createNamedQuery("ClientRequest.findById");
            query.setParameter("id", id);
            return (ClientRequest) query.getSingleResult();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientTransaction getClientTransaction(Long id) {
        try {
            Query query = em.createNamedQuery("ClientTransaction.findById");
            query.setParameter("id", id);
            return (ClientTransaction) query.getSingleResult();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientTransaction getTxnTypeClientTxn(Long clientRequestId, String txnType) {
        try {
            Query query = em.createNamedQuery("ClientTransaction.findByRequestIdAndTxnType");
            query.setParameter("clientRequestId", clientRequestId);
            query.setParameter("txnType", txnType);
            return (ClientTransaction) query.getSingleResult();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientRequest checkClientRequestForDuplicity(Long serviceId, String requestId, Long clientId) {
        try {
            Query query = em.createNamedQuery("ClientRequest.findDuplicateRequest");
            query.setParameter("requestId", requestId);
            query.setParameter("serviceId", serviceId);
            query.setParameter("clientId", clientId);
            query.setMaxResults(1);
            ClientRequest clientRequest = (ClientRequest) query.getSingleResult();
            return clientRequest;
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientRequest checkDuplicatePayment(Long clientId, String serviceAttribute) {
        try {
            Query query = em.createNamedQuery("ClientRequest.checkDuplicatePayment");
            query.setParameter("clientId", clientId);
            query.setParameter("serviceAttribute", serviceAttribute);
            query.setParameter("status", 2);
            query.setMaxResults(1);
            ClientRequest clientRequest = (ClientRequest) query.getSingleResult();
            return clientRequest;
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public ClientRequestResult latestRequestResult(Long clientRequestId) {
        try {
            Query query = em.createNamedQuery("ClientRequestResult.findByClientRequestId");
            query.setParameter("clientRequestId", clientRequestId);
            query.setMaxResults(1);
            ClientRequestResult clientRequestResult = (ClientRequestResult) query.getSingleResult();
            return clientRequestResult;
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return null;
    }

    @Override
    public List<ServiceResponseInfo> getServiceResponseInfoList() {
        try {
            Query query = em.createNamedQuery("ServiceResponseInfo.findAll");
            return query.getResultList();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return new ArrayList<>();
    }

    @Override
    public List<ApplicationConfig> getApplicationConfigList() {
        try {
            Query query = em.createNamedQuery("ApplicationConfig.findAll");
            return query.getResultList();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return new ArrayList<>();
    }

    @Override
    public List<ProviderResultCode> getProviderResultCodeList() {
        try {
            Query query = em.createNamedQuery("ProviderResultCode.findAllActive");
            return query.getResultList();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return new ArrayList<>();
    }

    @Override
    public List<MPOSDefaultParameter> getMPOSDefaultParameters() {
        try {
            Query query = em.createNamedQuery("MPOSDefaultParameter.findAll");
            return query.getResultList();
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception ", e);
        }
        return new ArrayList<>();
    }

    @Override
    public ClientAccount getAccountByClientId(Long clientId) {
        try {
            Query query = em.createNamedQuery("ClientAccount.findByClientId");
            query.setParameter("clientId", clientId);
            query.setLockMode(LockModeType.PESSIMISTIC_READ);
            List<ClientAccount> clientAccountList = query.getResultList();

            if (clientAccountList != null && clientAccountList.size() > 0) {
                return clientAccountList.get(0);
            }
        } catch (NoResultException e) {
        } catch (Exception e) {
            log.error("Exception : ", e);
        }
        return null;
    }
}
