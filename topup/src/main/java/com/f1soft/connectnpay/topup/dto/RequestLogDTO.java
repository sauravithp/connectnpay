package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
public class RequestLogDTO extends ModelBase {

    private String amount;
    private String clientTraceId;
    private String channel;
    private String customerRefId;
    private String serviceAttribute;
    private String requestId;
    private String serviceCode;
    private String clientUsername;
    private String password;
    private String responseCode;
    private String responseDesc;
    private String type;
}
