package com.f1soft.connectnpay.topup.model;

import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Manjit Shakya <manjit.shakya@f1soft.com>
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "client_key_pair")
@Table(name = "CLIENT_KEY_PAIR")
@NamedQueries({
    @NamedQuery(name = "ClientKeyPair.findAll", query = "SELECT c FROM ClientKeyPair c")
    , @NamedQuery(name = "ClientKeyPair.findByClientId", query = "SELECT c FROM ClientKeyPair c WHERE c.client = :clientId")})
public class ClientKeyPair implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "SERVER_ENCRYPTION_PUBLIC_KEY", length = 500)
    private String serverEncryptionPublicKey;
    @Basic(optional = false)
    @Column(name = "SERVER_ENCRYPTION_PRIVATE_KEY", length = 2000)
    private String serverEncryptionPrivateKey;
    @Basic(optional = false)
    @Column(name = "SERVER_SIGNATURE_PUBLIC_KEY", length = 500)
    private String serverSignaturePublicKey;
    @Basic(optional = false)
    @Column(name = "SERVER_SIGNATURE_PRIVATE_KEY", length = 2000)
    private String serverSignaturePrivateKey;
    @Column(name = "CLIENT_SIGNATURE_PUBLIC_KEY", length = 500)
    private String clientSignaturePublicKey;
    @Basic(optional = false)
    @Column(name = "CLIENT_ENCRYPTION_PUBLIC_KEY", length = 500)
    private String clientEncryptionPublicKey;
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    public ClientKeyPair(Long id) {
        this.id = id;
    }
}
