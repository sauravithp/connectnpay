package com.f1soft.connectnpay.topup.constant;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class RequestResponseCode {
    public static final String PAYMENT_SUCCESSFUL = "SUCC";
    public static final String PAYMENT_FAILURE = "FAIL";
    public static final String INVALID_CLIENT_REQUEST = "ICR";
    public static final String CLIENT_NOT_ACTIVE = "CNA";
    public static final String CLIENT_AUTHENTICATION_FAILURE = "CAF";
    public static final String SERVICE_NOT_AVAILABLE = "SNA";
    public static final String REQUESTER_CLIENT_IP_RESTRICTED = "RCIR";
    public static final String CLIENT_ACCOUNT_NOT_FOUND = "CANF";
    public static final String EXCEPTION = "ERR";
    public static final String INSUFFICIENT_ACCOUNT_BALANCE = "IAB";
    public static final String INVALID_PAYMENT_CODE = "IPC";
    public static final String INVALID_TXN_AMOUNT = "ITA";
    public static final String SERVER_CONNECTION_TIMEOUT = "SCT";
    public static final String SUSPICIOUS_TRANSACTION = "SCPT";
    public static final String SERVICE_NOT_FOUND = "SNF";
    public static final String SERVICE_NOT_ASSIGNED = "SNASS";
    public static final String SERVICE_BLOCKED = "SB";
    public static final String SERVICE_ACTIVE_AND_ASSIGNED = "SAA";
    public static final String VENDOR_LOGIN_FAILED = "VLF";
    public static final String TXN_REVERSAL_SUCCESS = "TRS";
    public static final String TXN_REVERSAL_FAILURE = "TRF";
    public static final String INVALID_REQUEST_PARAMTER = "IRP";
    public static final String DUPLICATE_REQUEST_ID = "DRI";
    public static final String CLIENT_PROFILE_BLOCKED = "CPB";
    public static final String BALANCE_LOAD_SUCCESS = "BLS";
    public static final String BALANCE_LOAD_FAILURE = "BLF";
    public static final String INVALID_REQUEST = "IR";
    public static final String RESPONSE_NOT_AVAILABLE = "RNA";
    public static final String AUTHENTICATION_FAILURE = "AF";
    public static final String DUPLICATE_PAYMENT_DETAIL = "DPD";
    public static final String SERVICE_VALIDATION_SUCCESS = "SVS";
    public static final String PAYMENT_VALIDATION_SUCCESS = "PVS";
    public static final String NO_DUE_PENDING = "NDP";
    public static final String COOPERATIVE_WALLET_MSSING = "COOPWM";
    public static final String SUBMIT_SUCCESS = "SUBM_SUCC";
}
