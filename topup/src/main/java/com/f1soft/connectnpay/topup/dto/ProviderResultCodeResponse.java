package com.f1soft.connectnpay.topup.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class ProviderResultCodeResponse {
    private Map<String, String> successCodeMap;
    private Map<String, String> timeoutCodeMap;
    private Map<String, String> failureCodeMap;

    public Map<String, String> getSuccessCodeMap() {
        if (successCodeMap == null) {
            successCodeMap = new HashMap<>();
        }
        return successCodeMap;
    }

    public void setSuccessCodeMap(Map<String, String> successCodeMap) {
        this.successCodeMap = successCodeMap;
    }

    public Map<String, String> getTimeoutCodeMap() {
        if (successCodeMap == null) {
            successCodeMap = new HashMap<>();
        }
        return timeoutCodeMap;
    }

    public void setTimeoutCodeMap(Map<String, String> timeoutCodeMap) {
        this.timeoutCodeMap = timeoutCodeMap;
    }

    public Map<String, String> getFailureCodeMap() {
        if (successCodeMap == null) {
            successCodeMap = new HashMap<>();
        }
        return failureCodeMap;
    }

    public void setFailureCodeMap(Map<String, String> failureCodeMap) {
        this.failureCodeMap = failureCodeMap;
    }
}
