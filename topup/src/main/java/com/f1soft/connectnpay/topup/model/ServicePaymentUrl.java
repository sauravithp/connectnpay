package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author yogesh
 */
@Entity
@Cacheable(true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "service_payment_url")
@Table(name = "SERVICE_PAYMENT_URL")
@NamedQueries({
    @NamedQuery(name = "ServicePaymentUrl.findAll", query = "SELECT c FROM ServicePaymentUrl c"),
    @NamedQuery(name = "ServicePaymentUrl.findById", query = "SELECT c FROM ServicePaymentUrl c WHERE c.id = :id")
})
public class ServicePaymentUrl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "URL")
    private String url;
    @Basic(optional = false)
    @Column(name = "HAS_DEPENDENT_URL", nullable = false, length = 1)
    private char hasDependentUrl;
    @Column(name = "CALL_ORDER")
    private String callOrder;
    @Column(name = "DEPENDENT_ATTRIBUTE")
    private String dependentAttribute;
    @Column(name = "DEPENDENT_URL")
    private String dependentUrl;
    @JoinColumn(name = "SERVICE_ID")
    @OneToOne(fetch = FetchType.LAZY)
    private Service service;
    @Column(name = "REQUEST_TYPE")
    private String requestType;
    @Column(name = "SUB_TYPE")
    private String subType;

    public ServicePaymentUrl() {
    }

    public ServicePaymentUrl(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public char getHasDependentUrl() {
        return hasDependentUrl;
    }

    public void setHasDependentUrl(char hasDependentUrl) {
        this.hasDependentUrl = hasDependentUrl;
    }

    public String getCallOrder() {
        return callOrder;
    }

    public void setCallOrder(String callOrder) {
        this.callOrder = callOrder;
    }

    public String getDependentUrl() {
        return dependentUrl;
    }

    public void setDependentUrl(String dependentUrl) {
        this.dependentUrl = dependentUrl;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getDependentAttribute() {
        return dependentAttribute;
    }

    public void setDependentAttribute(String dependentAttribute) {
        this.dependentAttribute = dependentAttribute;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
}
