package com.f1soft.connectnpay.topup.util;

import com.f1soft.connectnpay.topup.dto.NTTransactionVerificationRequest;
import com.f1soft.connectnpay.topup.dto.TxnVerifyParameter;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author Sauravi Thapa
 * 4/11/21
 */
@Getter
@Setter
public class ConvertUtil {

    public static TxnVerifyParameter convertToNTTxnStatusParameter(NTTransactionVerificationRequest enquiryRequest,
                                                                   String header) {
        TxnVerifyParameter parameter = new TxnVerifyParameter();
        parameter.setTraceId(enquiryRequest.getTraceId());
        parameter.setObj(enquiryRequest);
        parameter.setToken(header);
        return parameter;
    }

}
