package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity
@Cacheable(true)
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "service_scheme")
@Table(name = "SERVICE_SCHEME")
public class ServiceScheme implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "SCHEME_CODE")
    private Character schemeCode;
    @Basic(optional = false)
    @Column(name = "CALCULATE_IN")
    private Character calculateType;
    @Basic(optional = false)
    @Column(name = "CALCULATE_RATE")
    private String calculateRate;
    @Basic(optional = false)
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @Basic(optional = false)
    @JoinColumn(name = "SERVICE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Service service;
    @Basic(optional = false)
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    public ServiceScheme() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(Character schemeCode) {
        this.schemeCode = schemeCode;
    }

    public String getCalculateRate() {
        return calculateRate;
    }

    public void setCalculateRate(String calculateRate) {
        this.calculateRate = calculateRate;
    }

    public Character getCalculateType() {
        return calculateType;
    }

    public void setCalculateType(Character calculateType) {
        this.calculateType = calculateType;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ServiceScheme[id=" + id + "]";
    }
}
