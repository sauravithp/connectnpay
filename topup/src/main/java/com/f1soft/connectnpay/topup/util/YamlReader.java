package com.f1soft.connectnpay.topup.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.representer.Representer;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class YamlReader {
    static final Logger logger = LoggerFactory.getLogger(YamlReader.class);

    public <T> List<T> getAll(String fileName, Class<T> clazz) {
        Representer representer = new Representer();
//        representer.getPropertyUtils().setSkipMissingProperties(true);
        Yaml yaml = new Yaml(new Constructor(clazz), representer);
        List<T> list = new ArrayList<>();
        try {
            InputStream in = new FileInputStream(new File(fileName));

            Iterable<Object> loadAll = yaml.loadAll(in);

            for (Object load : loadAll) {
                T object = clazz.cast(load);
                list.add(object);
            }
        } catch (Exception e) {
            logger.error("Exception ", e);
        }
        return list;
    }

    public <T> T load(String fileName, Class<T> clazz) {
        Representer representer = new Representer();
        Yaml yaml = new Yaml(new Constructor(clazz), representer);
        try {
            return yaml.loadAs(new FileInputStream(fileName), clazz);
        } catch (Exception e) {
            logger.error("Exception : ", e);
            logger.error("Configuration is invalid in " + fileName + " !!!!!!!!!!!!!!!!!!!!!!!!!");
            throw new RuntimeException("Error in configuration file");
        }
    }
}
