package com.f1soft.connectnpay.topup.configuration;

import com.f1soft.connectnpay.topup.dto.ModelBase;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class TtlConfig extends ModelBase {

    private int service;
    private int client;
    private int counter;
    private int defaultTtl;
}
