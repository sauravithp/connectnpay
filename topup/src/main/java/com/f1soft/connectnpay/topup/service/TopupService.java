package com.f1soft.connectnpay.topup.service;


import com.f1soft.connectnpay.topup.dto.*;

/**
 *
 * @author santosh
 */
public interface TopupService {

    ServerResponse doPayment(RequestDTO requestDTO, String clientIp);

    ServerResponse validatePayment(RequestDTO requestDTO, String clientIp);

    ServerResponse validateService(RequestDTO requestDTO);

    ServerResponse loadClientBalance(LoadClientBalanceRequest loadClientBalanceRequest, String header);

    ServerResponse reverseTransaction(RequestDTO requestDTO);

    ServerResponse changeStatus(ChangeStatusParameter changeStatusParameter);

    ServerResponse settleEsewa(Long clientRequestId, Long initiatorId);

    ServerResponse transactionStatus(TransactionStatusDTO requestDTO);

    ServerResponse serviceStatus(RequestDTO requestDTO);

    ServerResponse doTxnVerification(TxnVerifyParameter request);

}
