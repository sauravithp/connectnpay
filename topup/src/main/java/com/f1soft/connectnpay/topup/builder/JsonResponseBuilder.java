package com.f1soft.connectnpay.topup.builder;

import com.f1soft.connectnpay.topup.dto.EnquiryResponse;
import com.f1soft.connectnpay.topup.dto.ServerResponse;

/**
 * @author Sauravi Thapa
 * 4/11/21
 */
public class JsonResponseBuilder {
    public static EnquiryResponse enquiryResponse(ServerResponse serverResponse) {
        EnquiryResponse apiResponse = new EnquiryResponse();
        apiResponse.setResultCode(serverResponse.getRespCode());
        apiResponse.setResultDescription(serverResponse.getMessage());
        return apiResponse;
    }
}
