package com.f1soft.connectnpay.topup.util;

import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Slf4j
public class DateHelper {
    public static final String sqlDateFormat = "yyyy-MM-dd";
    public static final String TIMESTAMP_FORMAT = "yyMMddHHmmss";
    public static final String DATE_FORMAT_YYYYMMDD_TIME24 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_YYYMMDDTHHMMSS = "yyyy-MM-dd'T'HH:mm:ss";

    public static DateTime currentDateTime() {
        DateTime dateTime = DateTime.now();
        return dateTime;
    }

    public static java.sql.Date utilDateToSqlDate(java.util.Date uDate) {
        DateFormat sqlDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return java.sql.Date.valueOf(sqlDateFormatter.format(uDate));
    }

    public static String convertToString(DateTime dt, String format) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format);
        return fmt.print(dt);
    }

    public static DateTime convertToDate(String date, String format) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format);
        return fmt.parseDateTime(date);
    }

    public static String convertToString(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static Date changeFormat(Date date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            String formatedDate = simpleDateFormat.format(date);
            return simpleDateFormat.parse(formatedDate);
        } catch (ParseException e) {
            log.error("Exception", e);
            return null;
        }
    }

    public static long getMinutesDifference(Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        long min = diff / (60 * 1000);
        return min;
    }

    public static long getSecondsDifference(Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        long min = diff / 1000;
        return min;
    }

    public static DateTime addDays(DateTime dt, int no) {
        return dt.plus(Period.days(no));
    }

    public static DateTime addMonths(DateTime dt, int no) {
        return dt.plus(Period.months(no));
    }

    public static DateTime addMinutes(DateTime dt, int no) {
        return dt.plus(Period.minutes(no));
    }

    public static DateTime addSeconds(DateTime dt, int no) {
        return dt.plus(Period.seconds(no));
    }

    public static int getDayOfWeek(DateTime dt) {
        return dt.getDayOfWeek();
    }

    public static int getHourOfDay(DateTime dt) {
        return dt.getHourOfDay();
    }

    public static int getMinuteOfHour(DateTime dt) {
        return dt.getMinuteOfHour();
    }

    public static String getMonthName(Date date, boolean shortName) {
        DateTime dt = new DateTime();
        if (shortName) {
            return dt.monthOfYear().getAsShortText();
        } else {
            return dt.monthOfYear().getAsText();
        }
    }

    public static java.util.Date addDay(java.util.Date oldDate, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(oldDate);
        calendar.add(5, days);
        return calendar.getTime();
    }

    public static DateTime addHours(DateTime dt, int no) {
        return dt.plus(Period.hours(no));
    }

    public static boolean matchDate(LocalDate date1, LocalDate date2) {
        return date1.equals(date2);
    }

    public static long timeDiff(Date startDateTime) {
        Date endDate = new Date();
        long diff = (endDate.getTime() - startDateTime.getTime());
        return diff;
    }

    public static String elapsedTime(long elapsedTime) {
        try {
            //convert minute to second
            long minuteInSecond = 60;

            //convert hour to second
            long hourInSecond = minuteInSecond * 60;

            //convert day to second
            long daysInSecond = hourInSecond * 24;

            //convert year to second
            long yearInSecond = daysInSecond * 365;

            long elapsedYears = elapsedTime / yearInSecond;

            elapsedTime = elapsedTime % yearInSecond;

            long elapsedDays = elapsedTime / daysInSecond;

            elapsedTime = elapsedTime % daysInSecond;

            long elapsedHours = elapsedTime / hourInSecond;

            elapsedTime = elapsedTime % hourInSecond;

            long elapsedMinutes = elapsedTime / minuteInSecond;

            elapsedTime = elapsedTime % minuteInSecond;

            long elapsedSeconds = elapsedTime;

            String elapsedPeriod = "";

            if (elapsedDays > 0) {
                elapsedPeriod = elapsedDays + " Day ";
            }

            if (elapsedHours > 0) {
                if (elapsedHours == 1) {
                    elapsedPeriod += elapsedHours + " Hour ";
                } else {
                    elapsedPeriod += elapsedHours + " Hours ";
                }
            }

            if (elapsedMinutes > 0) {
                if (elapsedMinutes == 1) {
                    elapsedPeriod += elapsedMinutes + " Minute ";
                } else {
                    elapsedPeriod += elapsedMinutes + " Minutes ";
                }
            }

            if (elapsedSeconds < 60) {
                if (elapsedSeconds == 1) {
                    elapsedPeriod += elapsedSeconds + " Second ";
                } else {
                    elapsedPeriod += elapsedSeconds + " Seconds ";
                }
            }

            return elapsedPeriod;
        } catch (Exception e) {
            log.error("Exception ", e);
            return null;
        }
    }
}
