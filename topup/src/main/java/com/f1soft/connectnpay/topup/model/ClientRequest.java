package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity()
@Table(name = "CLIENT_REQUEST", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"REQUEST_ID", "SERVICE_ID", "CLIENT_ID"})})
@NamedQueries({
    @NamedQuery(name = "ClientRequest.findAll", query = "SELECT b FROM ClientRequest b")
    ,
    @NamedQuery(name = "ClientRequest.findById", query = "SELECT b FROM ClientRequest b WHERE b.id = :id")
    ,
    @NamedQuery(name = "ClientRequest.findByRequestId", query = "SELECT b FROM ClientRequest b WHERE b.requestIdRef = :requestId order by b.id desc")
    ,
    @NamedQuery(name = "ClientRequest.findDuplicateRequest", query = "SELECT b FROM ClientRequest b where b.requestIdRef = :requestId AND b.service.id = :serviceId AND b.client.id= :clientId order by b.id desc")
    ,
    @NamedQuery(name = "ClientRequest.checkDuplicatePayment", query = "SELECT b FROM ClientRequest b where b.client.id= :clientId AND b.serviceAttribute = :serviceAttribute AND b.status = :status order by b.id desc")
})
public class ClientRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "REQUEST_ID", length = 50)
    private String requestId;
    @Column(name = "REQUEST_ID_REF", length = 50)
    private String requestIdRef;
    @Basic(optional = false)
    @Column(name = "CLIENT_TRACE_ID", length = 50)
    private String clientTraceId;
    @Basic(optional = false)
    @Column(name = "SERVICE_ATTRIBUTE", length = 150)
    private String serviceAttribute;
    @Column(name = "CHANNEL", length = 50)
    private String channel;
    @Column(name = "CUSTOMER_REF_ID", length = 80)
    private String customerRefId;
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "client")
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_ID")
    @ManyToOne()
    private Client client;
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service")
    @Basic(optional = false)
    @JoinColumn(name = "SERVICE_ID")
    @ManyToOne()
    private Service service;
    @Basic(optional = false)
    @Column(name = "REQUESTED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestedDate;
    @Basic(optional = false)
    @Column(name = "AMOUNT", precision = 19, scale = 2)
    private BigDecimal amount;
    @Basic(optional = false)
    @Column(name = "TXN_STATUS", length = 1)
    private Integer txnStatus;
    @Column(name = "TXN_ERROR_CODE", length = 10)
    private String txnErrorCode;
    @Column(name = "TXN_ERROR_DESC", length = 200)
    private String txnErrorDesc;
    @Basic(optional = false)
    @Column(name = "STATUS", length = 1)
    private Integer status;
    @Column(name = "SERVICE_CODE")
    private String serviceCode;
    @Column(name = "REQUEST_ATTEMPT")
    private Integer requestAttempt;
    @Column(name = "VENDOR_AMOUNT", precision = 19, scale = 2)
    private BigDecimal vendorAmount;
    @Column(name = "SYSTEM_ERROR_CODE", length = 60)
    private String sytemErrorCode;
    @Column(name = "TXN_API_SETTLEMENT", length = 2)
    private String txnApiSettlement;
    @Column(name = "VENDOR_TRACE_ID")
    private String vendorTraceId;
    @Column(name = "VENDOR_RESPONSE_CODE", length = 10)
    private String vendorResponseCode;
    @Column(name = "SERVICE_PAYMENT_URL_ID", precision = 22)
    private Long servicePaymentUrlId;
    @Column(name = "INITIATOR_ID", precision = 22)
    private Long initiatorId;
    @Column(name = "CLIENT_REQUEST_ID", precision = 22)
    private Long clientRequestId;
    @Column(name = "VENDOR_TRANS_ID", length = 50)
    private String vendorTransId;
    @Transient
    private String dbExceptionCode;

    public ClientRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getClientTraceId() {
        return clientTraceId;
    }

    public void setClientTraceId(String clientTraceId) {
        this.clientTraceId = clientTraceId;
    }

    public String getCustomerRefId() {
        return customerRefId;
    }

    public void setCustomerRefId(String customerRefId) {
        this.customerRefId = customerRefId;
    }

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getServiceAttribute() {
        return serviceAttribute;
    }

    public void setServiceAttribute(String serviceAttribute) {
        this.serviceAttribute = serviceAttribute;
    }

    public String getTxnErrorCode() {
        return txnErrorCode;
    }

    public void setTxnErrorCode(String txnErrorCode) {
        this.txnErrorCode = txnErrorCode;
    }

    public String getTxnErrorDesc() {
        return txnErrorDesc;
    }

    public void setTxnErrorDesc(String txnErrorDesc) {
        this.txnErrorDesc = txnErrorDesc;
    }

    public Integer getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(Integer txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Integer getRequestAttempt() {
        return requestAttempt;
    }

    public void setRequestAttempt(Integer requestAttempt) {
        this.requestAttempt = requestAttempt;
    }

    public String getDbExceptionCode() {
        return dbExceptionCode;
    }

    public void setDbExceptionCode(String dbExceptionCode) {
        this.dbExceptionCode = dbExceptionCode;
    }

    public BigDecimal getVendorAmount() {
        return vendorAmount;
    }

    public void setVendorAmount(BigDecimal vendorAmount) {
        this.vendorAmount = vendorAmount;
    }

    public String getVendorTraceId() {
        return vendorTraceId;
    }

    public void setVendorTraceId(String vendorTraceId) {
        this.vendorTraceId = vendorTraceId;
    }

    public String getVendorResponseCode() {
        return vendorResponseCode;
    }

    public void setVendorResponseCode(String vendorResponseCode) {
        this.vendorResponseCode = vendorResponseCode;
    }

    public String getSytemErrorCode() {
        return sytemErrorCode;
    }

    public void setSytemErrorCode(String sytemErrorCode) {
        this.sytemErrorCode = sytemErrorCode;
    }

    public String getTxnApiSettlement() {
        return txnApiSettlement;
    }

    public void setTxnApiSettlement(String txnApiSettlement) {
        this.txnApiSettlement = txnApiSettlement;
    }

    public Long getServicePaymentUrlId() {
        return servicePaymentUrlId;
    }

    public void setServicePaymentUrlId(Long servicePaymentUrlId) {
        this.servicePaymentUrlId = servicePaymentUrlId;
    }

    public Long getInitiatorId() {
        return initiatorId;
    }

    public void setInitiatorId(Long initiatorId) {
        this.initiatorId = initiatorId;
    }

    public Long getClientRequestId() {
        return clientRequestId;
    }

    public void setClientRequestId(Long clientRequestId) {
        this.clientRequestId = clientRequestId;
    }

    public String getRequestIdRef() {
        return requestIdRef;
    }

    public void setRequestIdRef(String requestIdRef) {
        this.requestIdRef = requestIdRef;
    }

    public String getVendorTransId() {
        return vendorTransId;
    }

    public void setVendorTransId(String vendorTransId) {
        this.vendorTransId = vendorTransId;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ClientRequest[id=" + id + "]";
    }
}
