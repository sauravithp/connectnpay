package com.f1soft.connectnpay.topup.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
public class TransactionCheckStatusRequest extends ModelBase {

    @NotNull
    private String clientusername;
    @NotNull
    private String password;
    @NotNull
    private List<CheckStatusTxnList> transactions;
}
