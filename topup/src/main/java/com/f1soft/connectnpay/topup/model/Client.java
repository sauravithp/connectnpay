package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author yogesh
 */
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "client")
@Table(name = "CLIENT")
@NamedQueries({
        @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
        ,
        @NamedQuery(name = "Client.findById", query = "SELECT c FROM Client c WHERE c.id = :id")
        ,
        @NamedQuery(name = "Client.findByUsername", query = "SELECT c FROM Client c WHERE c.username = :username")
        ,
        @NamedQuery(name = "Client.findByRefCode", query = "SELECT c FROM Client c WHERE c.refCode = :refCode")
        ,
        @NamedQuery(name = "Client.findByActive", query = "SELECT c FROM Client c WHERE c.active = :active")
        ,
        @NamedQuery(name = "Client.findByClientKeyActiveAuth", query = "SELECT c FROM Client c WHERE c.clientKey = :clientKey AND c.active = :active AND c.authMode = :authMode")
        ,
        @NamedQuery(name = "Client.findValidClient", query = "SELECT c FROM Client c WHERE c.username = :username and c.password =:password and c.active = :active")
        ,
        @NamedQuery(name = "Client.findByClientKey", query = "SELECT c FROM Client c WHERE c.clientKey = :clientKey and c.active = :active")})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "USERNAME", length = 50)
    private String username;
    @Basic(optional = false)
    @Column(name = "PASSWORD", length = 50)
    private String password;
    @Basic(optional = false)
    @Column(name = "REF_CODE", length = 20)
    private String refCode;
    @Basic(optional = false)
    @Column(name = "NAME", length = 150)
    private String name;
    @Basic(optional = false)
    @Column(name = "ADDRESS", length = 150)
    private String address;
    @Column(name = "CONTACT_INFO", length = 50)
    private String contactInfo;
    @Basic(optional = false)
    @Column(name = "ACTIVE", length = 1)
    private char active;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "country")
    @Basic(optional = false)
    @JoinColumn(name = "COUNTRY_ID", referencedColumnName = "ID")
    @ManyToOne()
    private Country country;
    @Column(name = "API_SECRET", length = 500)
    private String apiSecret;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "profile")
    @Basic(optional = false)
    @JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID")
    @ManyToOne()
    private Profile profile;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;
    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ClientAccount> clientAccounts;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "client_ip_address")
    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ClientAccessIp> clientIpAddressList;
    @Basic(optional = false)
    @Column(name = "VALIDATE_IP", length = 1)
    private Character validateIp;
    @Basic(optional = false)
    @Column(name = "ALLOW_MULTI_TXN", length = 1)
    private Character allowMultiTxn;
    @Column(name = "ALLOW_SAME_REQUEST_AFTER")
    private Integer allowSameRequestAfter;
    @Transient
    private boolean obtained;
    @Column(name = "AUTH_MODE")
    private String authMode;
    @Column(name = "CLIENT_KEY")
    private String clientKey;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "client_key_pair")
    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ClientKeyPair> clientKeyPairs;

    public Client() {
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public List<ClientAccount> getClientAccounts() {
        return clientAccounts;
    }

    public void setClientAccounts(List<ClientAccount> clientAccounts) {
        this.clientAccounts = clientAccounts;
    }

    public boolean isObtained() {
        return obtained;
    }

    public void setObtained(boolean obtained) {
        this.obtained = obtained;
    }

    public Character getValidateIp() {
        return validateIp;
    }

    public void setValidateIp(Character validateIp) {
        this.validateIp = validateIp;
    }

    public List<ClientAccessIp> getClientIpAddressList() {
        return clientIpAddressList;
    }

    public void setClientIpAddressList(List<ClientAccessIp> clientIpAddressList) {
        this.clientIpAddressList = clientIpAddressList;
    }

    public Integer getAllowSameRequestAfter() {
        return allowSameRequestAfter;
    }

    public void setAllowSameRequestAfter(Integer allowSameRequestAfter) {
        this.allowSameRequestAfter = allowSameRequestAfter;
    }

    public Character getAllowMultiTxn() {
        return allowMultiTxn;
    }

    public void setAllowMultiTxn(Character allowMultiTxn) {
        this.allowMultiTxn = allowMultiTxn;
    }

    public String getApiSecret() {
        return apiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }

    public String getAuthMode() {
        return authMode;
    }

    public void setAuthMode(String authMode) {
        this.authMode = authMode;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public List<ClientKeyPair> getClientKeyPairs() {
        return clientKeyPairs;
    }

    public void setClientKeyPairs(List<ClientKeyPair> clientKeyPairs) {
        this.clientKeyPairs = clientKeyPairs;
    }

    @Override
    public String toString() {
        return "com.f1soft.smsbanking.entities.Client[id=" + id + "]";
    }
}
