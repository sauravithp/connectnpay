package com.f1soft.connectnpay.topup.dao;

import com.f1soft.connectnpay.topup.model.*;

import java.util.List;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public interface FetchDataDAO {
    Client authenticateClient(String username);

    Service getService(Long serviceId);

    ClientAccount getClientAccountByClientId(Long clientId);

    ClientAccount getClientAccount(Long id);

    ClientRequest getClientRequestByRequestId(String requestId);

    ClientRequest getClientRequestById(Long id);

    ClientTransaction getClientTransaction(Long id);

    ClientTransaction getTxnTypeClientTxn(Long clientRequestId, String txnType);

    ClientRequest checkClientRequestForDuplicity(Long serviceId, String requestId, Long clientId);

    ClientRequest checkDuplicatePayment(Long clientId, String serviceAttribute);

    ClientRequestResult latestRequestResult(Long clientRequestId);

    List<ServiceResponseInfo> getServiceResponseInfoList();

    List<ApplicationConfig> getApplicationConfigList();

    List<ProviderResultCode> getProviderResultCodeList();

    List<MPOSDefaultParameter> getMPOSDefaultParameters();

    ClientAccount getAccountByClientId(Long clientId);
}
