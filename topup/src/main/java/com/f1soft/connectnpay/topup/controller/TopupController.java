package com.f1soft.connectnpay.topup.controller;

import com.f1soft.connectnpay.topup.builder.JsonResponseBuilder;
import com.f1soft.connectnpay.topup.builder.ResponseBuilder;
import com.f1soft.connectnpay.topup.builder.TopupResponseBuilder;
import com.f1soft.connectnpay.topup.constant.RequestResponseCode;
import com.f1soft.connectnpay.topup.dto.*;
import com.f1soft.connectnpay.topup.handler.TopupRequestHandler;
import com.f1soft.connectnpay.topup.service.TopupService;
import com.f1soft.connectnpay.topup.util.ConvertUtil;
import com.f1soft.connectnpay.topup.util.WebUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.f1soft.connectnpay.topup.util.WebUtils.getClientIpAddress;

@Slf4j
@RestController("api")
@AllArgsConstructor
public class TopupController extends BaseController {

    private final TopupRequestHandler topupRequestHandler;

    private final TopupService topupService;


    @GetMapping(path = "echo", produces = MediaType.APPLICATION_JSON_VALUE)
    public String echo() {
        return "<h1 style='font-size:30px;color:#AA3939;text-align:center;margin-top:50px;'>" +
                "Topup server 2.0.0.0 is running</h1>";
    }

    @GetMapping(path = "ip", produces = javax.ws.rs.core.MediaType.TEXT_HTML)
    public String clientIp() {
        String proxyIp = requestContext.getHeader("X-FORWARDED-FOR");
        String ipAddress = requestContext.getRemoteAddr();
        return "<h1 style='font-size:40px;color:#AA3939;text-align:center;margin-top:50px;'>Your IP address is "
                + ipAddress + " and proxy IP address is " + proxyIp + "</h1>";
    }

    @GetMapping(consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response doTopup() {
        RequestDTO requestDTO = topupRequestHandler.convertToRequestDTO(uri.getQueryParameters());
        try {
            ServerResponse serverResponse = topupService.doPayment(requestDTO, getClientIpAddress(requestContext));

            if (serverResponse.isSuccess()) {
                BillPayResponse billPayResponse = (BillPayResponse) serverResponse.getObj();
                return Response.status(Response.Status.OK)
                        .entity(billPayResponse)
                        .build();
            } else {
                return Response.status(Response.Status.OK)
                        .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                        .build();
            }
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                    .build();
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response doTopup(@Valid BillPayRequest billPayRequest) {
        RequestDTO requestDTO = topupRequestHandler.convertToRequestDTO(billPayRequest);
        try {
            ServerResponse serverResponse = topupService.doPayment(requestDTO,
                    WebUtils.getClientIpAddress(requestContext));

            if (serverResponse.isSuccess()) {
                BillPayResponse billPayResponse = (BillPayResponse) serverResponse.getObj();
                return Response.status(Response.Status.OK)
                        .entity(billPayResponse)
                        .build();
            } else {
                return Response.status(Response.Status.OK)
                        .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                        .build();
            }
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                    .build();
        }
    }

    @GetMapping(path = "validatePayment",
            consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response bookPayment() {
        RequestDTO requestDTO = topupRequestHandler.convertToRequestDTO(uri.getQueryParameters());
        try {
            ServerResponse serverResponse = topupService.validatePayment(requestDTO,
                    WebUtils.getClientIpAddress(requestContext));

            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                    .build();
        }
    }


    @PostMapping(path = "validatePayment",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response bookPayment(@Valid BillPayRequest billPayRequest) {
        RequestDTO requestDTO = topupRequestHandler.convertToRequestDTO(billPayRequest);
        try {
            ServerResponse serverResponse = topupService.validatePayment(requestDTO,
                    WebUtils.getClientIpAddress(requestContext));

            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.paymentResponse(serverResponse, requestDTO))
                    .build();
        }
    }

    @GetMapping(path = "validateService",
            consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response validateService() {
        RequestDTO requestDTO = topupRequestHandler.convertServiceValidationParameters(uri.getQueryParameters());
        try {
            ServerResponse serverResponse = topupService.validateService(requestDTO);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    //    @DefaultSecurity
    @PostMapping(path = "load_client_balance",
            consumes = javax.ws.rs.core.MediaType.APPLICATION_JSON,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response loadBalance(LoadClientBalanceRequest loadClientBalanceRequest) {
        try {
            ServerResponse serverResponse = topupService.loadClientBalance(loadClientBalanceRequest,
                    requestContext.getHeader("Authorization"));
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(), RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    @GetMapping(path = "reversal_request",
            consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response reverseTransaction() {
        RequestDTO requestDTO = topupRequestHandler.convertReversalParameters(uri.getQueryParameters());
        try {
            ServerResponse serverResponse = topupService.reverseTransaction(requestDTO);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    @GetMapping(path = "change_txn_status",
            consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response changeStatus() {
        ChangeStatusParameter changeStatusParameter = topupRequestHandler
                .convertToChangeStatusParameter(uri.getQueryParameters());
        try {
            ServerResponse serverResponse = topupService.changeStatus(changeStatusParameter);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    @GetMapping(path = "esewa_settlement",
            consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response esewaSettlement() {
        Long clientRequestId = Long.parseLong(uri.getQueryParameters().getFirst("clientRequestId"));
        Long initiatorId = Long.parseLong(uri.getQueryParameters().getFirst("initiatorId"));
        log.debug("Client request id : " + clientRequestId);
        log.debug("Initiator id : " + initiatorId);
        try {
            ServerResponse serverResponse = topupService.settleEsewa(clientRequestId, initiatorId);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    @GetMapping(path = "check_txn_status",
            consumes = javax.ws.rs.core.MediaType.TEXT_PLAIN,
            produces = javax.ws.rs.core.MediaType.APPLICATION_XML)
    public Response transactionStatus() {
        TransactionStatusDTO requestDTO = topupRequestHandler.convertTransactionStatusParameters(uri.getQueryParameters());
        log.debug("Request dto : " + requestDTO);
        try {
            ServerResponse serverResponse = topupService.transactionStatus(requestDTO);
            if (serverResponse.isSuccess()) {
                List<BillPayResponse> billPayResponse = (List<BillPayResponse>) serverResponse.getObj();
                return Response.status(Response.Status.OK)
                        .entity(billPayResponse.get(0))
                        .build();
            } else {
                return Response.status(Response.Status.NOT_ACCEPTABLE)
                        .entity(TopupResponseBuilder.buildResponse(serverResponse,
                                requestDTO.getTransactions().get(0).getRequestId()))
                        .build();
            }
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.NOT_ACCEPTABLE)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    //    @DefaultSecurity
    @PostMapping(path = "check/status",
            consumes = javax.ws.rs.core.MediaType.APPLICATION_JSON,
            produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Response checkStatus(@Valid TransactionCheckStatusRequest statusRequest) {
        TransactionStatusDTO statusRequestDTO = topupRequestHandler
                .convertStatusRequestToStatusRequestDTO(statusRequest);
        log.debug("Status Request dto : " + statusRequestDTO);
        try {
            ServerResponse serverResponse = topupService.transactionStatus(statusRequestDTO);
            if (serverResponse.isSuccess()) {
                List<BillPayResponse> billPayResponse = (List<BillPayResponse>) serverResponse.getObj();
                return Response.status(Response.Status.OK)
                        .entity(billPayResponse)
                        .build();
            } else {
                return Response.status(Response.Status.OK)
                        .entity(TopupResponseBuilder.buildResponse(serverResponse))
                        .build();
            }
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    //    @DefaultSecurity
    @PostMapping(path = "serviceStatus",
            consumes = javax.ws.rs.core.MediaType.APPLICATION_JSON,
            produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Response serviceStatus(ServiceStatusRequest request) {
        try {
            RequestDTO requestDTO = topupRequestHandler.convertServiceStatusParameters(request);

            ServerResponse serverResponse = topupService.serviceStatus(requestDTO);
            ServiceStatusResponse serviceStatusResponse = (ServiceStatusResponse) serverResponse.getObj();
            if (serviceStatusResponse != null) {
                return Response.status(Response.Status.OK)
                        .entity(serviceStatusResponse)
                        .build();
            } else {
                return Response.status(Response.Status.OK)
                        .entity(TopupResponseBuilder.buildResponse(serverResponse))
                        .build();
            }
        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(TopupResponseBuilder.buildResponse(serverResponse))
                    .build();
        }
    }

    //    @DefaultSecurity
    @PostMapping(path = "transaction/verification",
            consumes = javax.ws.rs.core.MediaType.APPLICATION_JSON,
            produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public Response verifyTransaction(@Valid NTTransactionVerificationRequest request) {
        try {
            log.debug("NT Transaction Verification API Invoked with equest Param::: {}", request);
            String header = requestContext.getHeader("authorization");
            TxnVerifyParameter txnVerifyParameter = ConvertUtil.convertToNTTxnStatusParameter(request, header);
            ServerResponse serverResponse = topupService.doTxnVerification(txnVerifyParameter);

            return Response.status(Response.Status.OK)
                    .entity(JsonResponseBuilder.enquiryResponse(serverResponse))
                    .build();

        } catch (Exception e) {
            log.error("Exception ", e);
            ServerResponse serverResponse = ResponseBuilder.buildFailureMessage(new ServerResponse(),
                    RequestResponseCode.EXCEPTION);
            return Response.status(Response.Status.OK)
                    .entity(JsonResponseBuilder.enquiryResponse(serverResponse))
                    .build();
        }
    }

}
