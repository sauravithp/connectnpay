package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "CLIENT_ACCOUNT")
@NamedQueries({
    @NamedQuery(name = "ClientAccount.findAll", query = "SELECT b FROM ClientAccount b")
    ,
    @NamedQuery(name = "ClientAccount.findById", query = "SELECT b FROM ClientAccount b where b.id = :id")
    ,
    @NamedQuery(name = "ClientAccount.findByClientId", query = "SELECT b FROM ClientAccount b where b.client.id = :clientId")
})
public class ClientAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "country")
    @Basic(optional = false)
    @JoinColumn(name = "COUNTRY_ID", referencedColumnName = "ID")
    @ManyToOne()
    private Country country;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "currency")
    @Basic(optional = false)
    @JoinColumn(name = "CURRENCY_ID")
    @ManyToOne()
    private Currency currency;
    @Basic(optional = false)
    @Column(name = "BALANCE", precision = 19, scale = 2)
    private BigDecimal balance;
    @Basic(optional = false)
    @Column(name = "TOTAL_CHARGE", precision = 19, scale = 2)
    private BigDecimal totalCharge;
    @Basic(optional = false)
    @Column(name = "TOTAL_DISCOUNT", precision = 19, scale = 2)
    private BigDecimal totalDiscount;
    @Basic(optional = false)
    @Column(name = "ALLOW_CREDIT", length = 1)
    private Character allowCredit;
    @Column(name = "SETTLEMENT_ACCOUNT", length = 50)
    private String settlementAccount;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false, length = 1)
    private char active;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;
    @Column(name = "WALLET_MODE", length = 50)
    private String walletMode;

    public ClientAccount() {
    }

    public ClientAccount(Long id) {
        this.id = id;
    }

    public Character getAllowCredit() {
        return allowCredit;
    }

    public void setAllowCredit(Character allowCredit) {
        this.allowCredit = allowCredit;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getSettlementAccount() {
        return settlementAccount;
    }

    public void setSettlementAccount(String settlementAccount) {
        this.settlementAccount = settlementAccount;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public BigDecimal getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(BigDecimal totalCharge) {
        this.totalCharge = totalCharge;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getWalletMode() {
        return walletMode;
    }

    public void setWalletMode(String walletMode) {
        this.walletMode = walletMode;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ClientAccount[id=" + id + "]";
    }
}
