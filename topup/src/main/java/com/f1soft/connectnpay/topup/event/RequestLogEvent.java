package com.f1soft.connectnpay.topup.event;

import com.f1soft.connectnpay.topup.dto.RequestLog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
@AllArgsConstructor
public class RequestLogEvent {
    private RequestLog requestLog;
    private String responseCode;
    private String message;
}
