package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author yogesh
 */
@Entity()
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "client_ip_address")
@Table(name = "CLIENT_ACCESS_IP")
@NamedQueries({
    @NamedQuery(name = "ClientAccessIp.findAll", query = "SELECT b FROM ClientAccessIp b")
    ,
    @NamedQuery(name = "ClientAccessIp.findById", query = "SELECT b FROM ClientAccessIp b WHERE b.id = :id")
})
public class ClientAccessIp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;
    @Column(name = "IP_ADDRESS", length = 30)
    private String ipAddress;
    @Basic(optional = false)
    @Column(name = "ACTIVE", length = 1)
    private char active;

    public ClientAccessIp() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ClientRequest[id=" + id + "]";
    }
}
