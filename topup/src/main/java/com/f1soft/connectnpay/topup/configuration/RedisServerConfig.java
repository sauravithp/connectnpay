package com.f1soft.connectnpay.topup.configuration;

import com.f1soft.connectnpay.topup.dto.ModelBase;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class RedisServerConfig extends ModelBase {

    private String enableCaching;
    private String host;
    private String password;
    private int port;
    private int timeout;
    private int maxTotal;
    private int maxIdle;
    private int minIdle;
    private int maxWaitMillis;
    private boolean testOnBorrow;
    private boolean testOnReturn;
    private boolean testWhileIdle;
    private int minEvictableIdleTimeMillis;
    private int timeBetweenEvictionRunsMillis;
    private int numTestsPerEvictionRun;
    private TtlConfig ttl;

}
