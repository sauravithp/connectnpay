package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
public class ServerResponse extends BaseResponse {

    private String respCode;
    private String rawResponse;
    private String vendorRespCode;
    private String vendorRespDesc;
    private String txnCode;
    private boolean vendorResponseObtained;
    private Long vendorResponseTime;
    private String txnApiSettlement;
    private boolean obtained;
    private boolean valid;
    private boolean doReversal;
    private boolean paymentFailure;
    private String vendorTransId;
    private String serviceSettlementCode;
    private Object obj;

}
