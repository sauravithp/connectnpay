package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "MPOS_DEFAULT_PARAMETER")
@NamedQueries({
    @NamedQuery(name = "MPOSDefaultParameter.findAll", query = "SELECT b FROM MPOSDefaultParameter b"),
    @NamedQuery(name = "MPOSDefaultParameter.findById", query = "SELECT b FROM MPOSDefaultParameter b where b.id = :id")
})
public class MPOSDefaultParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "CONFIG_KEY", nullable = false, length = 50)
    private String configKey;
    @Column(name = "CONFIG_VALUE", length = 500)
    private String configValue;

    public MPOSDefaultParameter() {
    }

    public MPOSDefaultParameter(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MPOSDefaultParameter)) {
            return false;
        }
        MPOSDefaultParameter other = (MPOSDefaultParameter) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.MPOSDefaultParameter[id=" + id + "]";
    }
}
