package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author yogesh
 */
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "service")
@Table(name = "SERVICE")
@NamedQueries({
    @NamedQuery(name = "Service.findAll", query = "SELECT c FROM Service c")
    ,
    @NamedQuery(name = "Service.findById", query = "SELECT c FROM Service c WHERE c.id = :id")
    ,
    @NamedQuery(name = "Service.findByRefCode", query = "SELECT c FROM Service c WHERE c.refCode = :refCode")
    ,
    @NamedQuery(name = "Service.findByVendorCode", query = "SELECT c FROM Service c WHERE c.vendorCode = :vendorCode")
    ,
    @NamedQuery(name = "Service.findByActive", query = "SELECT c FROM Service c WHERE c.active = :active")
    ,
    @NamedQuery(name = "Service.findValidService", query = "SELECT c FROM Service c WHERE c.refCode = :refCode and c.active = :active")})
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @Column(name = "REF_CODE")
    private String refCode;
    @Column(name = "VENDOR_CODE")
    private String vendorCode;
    @Column(name = "CONTACT_NUMBER")
    private String contactNumber;
    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;
    @Basic(optional = false)
    @Column(name = "ACTIVE")
    private char active;
    @Basic(optional = false)
    @Column(name = "DO_SETTLEMENT", nullable = false, length = 1)
    private char doSettlement;
    @Basic(optional = false)
    @Column(name = "HAS_TXN_API", nullable = false, length = 1)
    private char hasTxnApi;
    @Basic(optional = false)
    @Column(name = "ALLOW_DECIMAL_LIMIT")
    private char allowDecimalLimit;
    @Column(name = "TXN_API_CODE")
    private String txnApiCode;
    @Basic(optional = false)
    @JoinColumn(name = "SERVICE_GROUP_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceGroup serviceGroup;
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "currency")
    @Basic(optional = false)
    @JoinColumn(name = "TXN_CURRENCY_ID")
    @OneToOne()
    private Currency currency;
    @Basic(optional = false)
    @JoinColumn(name = "COUNTRY_ID")
    @OneToOne(fetch = FetchType.LAZY)
    private Country country;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_payable_limit")
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ServicePayableLimit> servicePayableLimit;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_payment_parameter")
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ServicePaymentParameter> servicePaymentParameters;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_payment_url")
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ServicePaymentUrl> servicePaymentUrls;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_default_parameter")
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ServiceDefaultParameter> serviceDefaultParameters;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_scheme")
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    private List<ServiceScheme> serviceSchemes;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_credential")
    @OneToMany(mappedBy = "service", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ServiceCredential> serviceCredentials;
    @Column(name = "REVERSAL_VALID_DAY")
    private Integer reversalValidDay;
    @Column(name = "API_CONNECT_TIMEOUT")
    private Integer apiConnectTimeout;
    @Column(name = "API_RESPONSE_TIMEOUT")
    private Integer apiResponseTimeout;
    @Column(name = "WALLET_PAYMENT_CODE")
    private String walletPaymentCode;

    public Service() {
    }

    public Service(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public char getDoSettlement() {
        return doSettlement;
    }

    public void setDoSettlement(char doSettlement) {
        this.doSettlement = doSettlement;
    }

    public ServiceGroup getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(ServiceGroup serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public List<ServicePayableLimit> getServicePayableLimit() {
        return servicePayableLimit;
    }

    public void setServicePayableLimit(List<ServicePayableLimit> servicePayableLimit) {
        this.servicePayableLimit = servicePayableLimit;
    }

    public List<ServicePaymentParameter> getServicePaymentParameters() {
        return servicePaymentParameters;
    }

    public void setServicePaymentParameters(List<ServicePaymentParameter> servicePaymentParameters) {
        this.servicePaymentParameters = servicePaymentParameters;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public char getHasTxnApi() {
        return hasTxnApi;
    }

    public void setHasTxnApi(char hasTxnApi) {
        this.hasTxnApi = hasTxnApi;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<ServiceScheme> getServiceSchemes() {
        return serviceSchemes;
    }

    public void setServiceSchemes(List<ServiceScheme> serviceSchemes) {
        this.serviceSchemes = serviceSchemes;
    }

    public List<ServicePaymentUrl> getServicePaymentUrls() {
        return servicePaymentUrls;
    }

    public void setServicePaymentUrls(List<ServicePaymentUrl> servicePaymentUrls) {
        this.servicePaymentUrls = servicePaymentUrls;
    }

    public List<ServiceDefaultParameter> getServiceDefaultParameters() {
        return serviceDefaultParameters;
    }

    public void setServiceDefaultParameters(List<ServiceDefaultParameter> serviceDefaultParameters) {
        this.serviceDefaultParameters = serviceDefaultParameters;
    }

    public Integer getReversalValidDay() {
        return reversalValidDay;
    }

    public void setReversalValidDay(Integer reversalValidDay) {
        this.reversalValidDay = reversalValidDay;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public Integer getApiConnectTimeout() {
        return apiConnectTimeout;
    }

    public void setApiConnectTimeout(Integer apiConnectTimeout) {
        this.apiConnectTimeout = apiConnectTimeout;
    }

    public Integer getApiResponseTimeout() {
        return apiResponseTimeout;
    }

    public void setApiResponseTimeout(Integer apiResponseTimeout) {
        this.apiResponseTimeout = apiResponseTimeout;
    }

    public String getTxnApiCode() {
        return txnApiCode;
    }

    public void setTxnApiCode(String txnApiCode) {
        this.txnApiCode = txnApiCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public char getAllowDecimalLimit() {
        return allowDecimalLimit;
    }

    public void setAllowDecimalLimit(char allowDecimalLimit) {
        this.allowDecimalLimit = allowDecimalLimit;
    }

    public String getWalletPaymentCode() {
        return walletPaymentCode;
    }

    public void setWalletPaymentCode(String walletPaymentCode) {
        this.walletPaymentCode = walletPaymentCode;
    }

    public List<ServiceCredential> getServiceCredentials() {
        return serviceCredentials;
    }

    public void setServiceCredentials(List<ServiceCredential> serviceCredentials) {
        this.serviceCredentials = serviceCredentials;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.Service[id=" + id + "]";
    }
}
