package com.f1soft.connectnpay.topup.controller;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author santosh
 */
public abstract class BaseController {

    @Context
    protected UriInfo uri;

    @Context
    protected HttpServletRequest requestContext;

}
