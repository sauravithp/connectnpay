package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author Sauravi Thapa
 * 4/11/21
 */
@Getter
@Setter
public class ServiceStatusResponse implements Serializable{

    private String name;
    private String status;
    private String addedDate;
    private String lastModifiedDate;
    private List<PayableLimit> payableLimit;
}
