package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

/**
 * @author Sauravi Thapa
 * 4/11/21
 */
@Getter
@Setter
@XmlRootElement(name = "billPayResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"resultCode", "resultDescription"})
public class EnquiryResponse extends ModelBase  {
    @XmlElement(name = "resultCode")
    private String resultCode;
    @XmlElement(name = "resultDescription")
    private String resultDescription;
}
