package com.f1soft.connectnpay.topup.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author yogesh
 */
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "payment_code_type")
@Table(name = "PAYMENT_CODE_TYPE")
@NamedQueries({
    @NamedQuery(name = "PaymentCodeType.findAll", query = "SELECT p FROM PaymentCodeType p")
    ,
    @NamedQuery(name = "PaymentCodeType.findByActiveStatus", query = "SELECT p FROM PaymentCodeType p where p.active =:active")
})
public class PaymentCodeType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODE_TYPE_NAME", nullable = false, length = 50)
    private String codeTypeName;
    @Basic(optional = false)
    @Column(name = "ACTIVE", nullable = false)
    private Character active;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "payment_code_type_pattern")
    @OneToMany(mappedBy = "paymentCodeType", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<PaymentCodeTypePattern> paymentCodeTypePatternList;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;

    public PaymentCodeType() {
    }

    public PaymentCodeType(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeTypeName() {
        return codeTypeName;
    }

    public void setCodeTypeName(String codeTypeName) {
        this.codeTypeName = codeTypeName;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public ApplicationUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ApplicationUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ApplicationUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(ApplicationUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public List<PaymentCodeTypePattern> getPaymentCodeTypePatternList() {
        return paymentCodeTypePatternList;
    }

    public void setPaymentCodeTypePatternList(List<PaymentCodeTypePattern> paymentCodeTypePatternList) {
        this.paymentCodeTypePatternList = paymentCodeTypePatternList;
    }
}
