package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Sauravi Thapa
 * 4/11/21
 */

@Getter
@Setter
public class NTTransactionVerificationRequest extends ModelBase {
    @NotNull
    private Long initiatorId;
    @NotNull
    private String traceId;
    @NotNull
    private String transactionDate;
}
