package com.f1soft.connectnpay.topup.util;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Slf4j
public class WebUtils {
    public static String getClientIpAddress(HttpServletRequest requestContext) {
        String ipAddress = requestContext.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = requestContext.getRemoteAddr();
        }
        log.debug("Requested IP Address : " + ipAddress);
        return ipAddress;
    }
}
