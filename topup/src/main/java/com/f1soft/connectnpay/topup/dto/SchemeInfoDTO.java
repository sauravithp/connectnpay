package com.f1soft.connectnpay.topup.dto;

import com.f1soft.connectnpay.topup.model.ServiceScheme;
import lombok.Getter;
import lombok.Setter;

/**
 * @author santosh
 */
@Getter
@Setter
public class SchemeInfoDTO extends ModelBase {

    private Character schemeCode;
    private Character calculateType;
    private String calculateRate;
    private String clientSchemeRate;
    private double schemeAmount;
    private double clientSchemeAmount;
    private double providerSchemeAmount;
    private ServiceScheme serviceScheme;

}
