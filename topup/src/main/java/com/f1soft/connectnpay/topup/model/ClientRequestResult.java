package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity()
@Table(name = "CLIENT_REQUEST_RESULT")
@NamedQueries({
    @NamedQuery(name = "ClientRequestResult.findAll", query = "SELECT b FROM ClientRequestResult b")
    ,
    @NamedQuery(name = "ClientRequestResult.findByClientRequestId", query = "SELECT b FROM ClientRequestResult b WHERE b.clientRequest.id = :clientRequestId ORDER BY b.id DESC")
})
public class ClientRequestResult implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_REQUEST_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientRequest clientRequest;
    @Basic(optional = false)
    @Column(name = "RECORDED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date recordedDate;
    @Column(name = "VENDOR_TRACE_ID")
    private String vendorTraceId;
    @Column(name = "VENDOR_RESPONSE_CODE", length = 10)
    private String vendorResponseCode;
    @Column(name = "VENDOR_RESPONSE_DESC")
    private String vendorResponseDesc;
    @Column(name = "SYSTEM_CODE", length = 10)
    private String systemCode;
    @Column(name = "SYSTEM_DESC")
    private String systemDesc;
    @Column(name = "SYSTEM_TRACE_ID")
    private String systemTraceId;
    @Column(name = "VENDOR_RESPONSE")
    private String vendorResponse;
    @Column(name = "VENDOR_RESPONSE_TIME", precision = 22)
    private Long vendorResponseTime;
    @Column(name = "SYSTEM_RESPONSE_TIME", precision = 22)
    private Long systemResponseTime;
    @Column(name = "ADDITIONAL_RESPONSE")
    private String addtionalResponse;

    public ClientRequestResult() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVendorTraceId() {
        return vendorTraceId;
    }

    public void setVendorTraceId(String vendorTraceId) {
        this.vendorTraceId = vendorTraceId;
    }

    public String getVendorResponse() {
        return vendorResponse;
    }

    public void setVendorResponse(String vendorResponse) {
        this.vendorResponse = vendorResponse;
    }

    public String getVendorResponseCode() {
        return vendorResponseCode;
    }

    public void setVendorResponseCode(String vendorResponseCode) {
        this.vendorResponseCode = vendorResponseCode;
    }

    public String getVendorResponseDesc() {
        return vendorResponseDesc;
    }

    public void setVendorResponseDesc(String vendorResponseDesc) {
        this.vendorResponseDesc = vendorResponseDesc;
    }

    public ClientRequest getClientRequest() {
        return clientRequest;
    }

    public void setClientRequest(ClientRequest clientRequest) {
        this.clientRequest = clientRequest;
    }

    public Date getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(Date recordedDate) {
        this.recordedDate = recordedDate;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getSystemDesc() {
        return systemDesc;
    }

    public void setSystemDesc(String systemDesc) {
        this.systemDesc = systemDesc;
    }

    public String getSystemTraceId() {
        return systemTraceId;
    }

    public void setSystemTraceId(String systemTraceId) {
        this.systemTraceId = systemTraceId;
    }

    public Long getVendorResponseTime() {
        return vendorResponseTime;
    }

    public void setVendorResponseTime(Long vendorResponseTime) {
        this.vendorResponseTime = vendorResponseTime;
    }

    public Long getSystemResponseTime() {
        return systemResponseTime;
    }

    public void setSystemResponseTime(Long systemResponseTime) {
        this.systemResponseTime = systemResponseTime;
    }

    public String getAddtionalResponse() {
        return addtionalResponse;
    }

    public void setAddtionalResponse(String addtionalResponse) {
        this.addtionalResponse = addtionalResponse;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ClientRequestResult[id=" + id + "]";
    }
}
