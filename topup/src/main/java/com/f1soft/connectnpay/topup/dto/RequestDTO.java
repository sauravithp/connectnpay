package com.f1soft.connectnpay.topup.dto;


import com.f1soft.connectnpay.topup.model.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author santosh
 */
@Getter
@Setter
public class RequestDTO extends ModelBase {

    private BigDecimal amount;
    private String clientTraceId;
    private String channel;
    private String customerRefId;
    private String serviceAttribute;
    private Client client;
    private Service service;
    private String requestId;
    private String requestIdRef;
    private String serviceCode;
    private String clientUsername;
    private String password;
    private Long clientAccountId;
    private String remarks;
    private Long initiatorId;
    private Long serviceId;
    private String type;
    private ClientRequest clientRequest;
    private ClientTransaction clientTransaction;
    private String officeCode;
    private String bankCode;
    private String consumerId;
    private String scno;
    private SchemeInfoDTO schemeInfoDTO;
    private ServiceTxnParamDTO serviceTxnParamDTO;
    private boolean increaseRequestAttempt;
    private String customerNo;
    private String vendorCode;
    private String userId;
    //simtv transaction verification request
    private String rid;
    private String req;
    private String policyNo;
    private String uniqueIdGuid;
    private String monthId;
    private Integer packageId;
    private String paymentId;
    private String packageCode;
    private Long clientRequestId;
    //wallet id for identification of co-operative wallet account
    private String walletId;
    private String mobileNumber;
    //Darsantech policy info request
    private String insuranceCode;
    private String dateOfBirth;
    //Darsantech bill payment request
    private String transactionId;
    //Nepal life payment request
    private String token;
    //Midas Subscription request
    private String classId;
    private String noOfDays;
    //Prabhu tv payment request
    private String stockId;
    private String productId;
    private String dateLocale;
    private Object obj;
    private Date vendorStartDate;
    private Date requestLodgeDate;
    private boolean synchronous = true;
    private String bankId;
}
