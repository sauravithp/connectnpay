package com.f1soft.connectnpay.topup.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
@XmlRootElement(name = "BillPayResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"resultCode", "resultDescription", "requestId", "agentTransId", "systemTraceId", "mobileNumber", "amount"})
public class BillPayResponse extends ModelBase {

    @JsonProperty("resultCode")
    @XmlElement(name = "ResultCode")
    private String resultCode;
    @JsonProperty("resultDescription")
    @XmlElement(name = "ResultDescription")
    private String resultDescription;
    @JsonProperty("requestId")
    @XmlElement(name = "RequestId")
    private String requestId;
    @JsonProperty("mobileNo")
    @XmlElement(name = "MobileNo")
    private String mobileNumber;
    @JsonProperty("amount")
    @XmlElement(name = "Amount")
    private String amount;
    @JsonProperty("vendorTransId")
    @XmlElement(name = "VendorTransId")
    private String systemTraceId;
    @JsonProperty("agentTransId")
    @XmlElement(name = "AgentTransId")
    private String agentTransId;
}
