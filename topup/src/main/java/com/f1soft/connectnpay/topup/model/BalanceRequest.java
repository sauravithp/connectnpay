package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity()
@Table(name = "BALANCE_REQUEST")
public class BalanceRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @JoinColumn(name = "INITIATED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser initiatedBy;
    @Basic(optional = false)
    @Column(name = "INITIATED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date initiatedDate;
    @Basic(optional = false)
    @Column(name = "AMOUNT", precision = 19, scale = 2)
    private BigDecimal amount;
    @Column(name = "LOG_TYPE", length = 10)
    private String logType;
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_ACCOUNT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ClientAccount clientAccount;
    @Basic(optional = false)
    @Column(name = "TXN_STATUS", length = 1)
    private Integer txnStatus;
    @Basic()
    @Column(name = "TXN_ERROR_CODE", length = 10)
    private String txnErrorCode;
    @Basic()
    @Column(name = "TXN_ERROR_DESC", length = 50)
    private String txnErrorDesc;
    @Column(name = "REMARKS", length = 255)
    private String remarks;

    public BalanceRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public ApplicationUser getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(ApplicationUser initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public Date getInitiatedDate() {
        return initiatedDate;
    }

    public void setInitiatedDate(Date initiatedDate) {
        this.initiatedDate = initiatedDate;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getTxnErrorCode() {
        return txnErrorCode;
    }

    public void setTxnErrorCode(String txnErrorCode) {
        this.txnErrorCode = txnErrorCode;
    }

    public String getTxnErrorDesc() {
        return txnErrorDesc;
    }

    public void setTxnErrorDesc(String txnErrorDesc) {
        this.txnErrorDesc = txnErrorDesc;
    }

    public Integer getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(Integer txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
