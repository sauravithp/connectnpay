package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
public class TxnVerifyParameter extends ModelBase {

    private String token;
    private String traceId;
    private Object obj;
    private String channel;

}
