package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */

@Getter
@Setter
public class BaseResponse extends ModelBase {
    protected boolean success;
    protected String message;
    private String vendorTraceId;
}
