package com.f1soft.connectnpay.topup.constant;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class ApplicationConfigConstant {
    public static final String CARD_PAY_URL = "CARD_PAY_URL";

    public static final String CARD_PAY_USERNAME = "CARD_PAY_USERNAME";

    public static final String CARD_PAY_PASSWORD = "CARD_PAY_PASSWORD";

    public static final String CARD_PAY_CONNECT_TIMEOUT = "CARD_PAY_CONNECT_TIMEOUT";

    public static final String CARD_PAY_READ_TIMEOUT = "CARD_PAY_READ_TIMEOUT";

    public static final String DUPLICATE_REQUEST_HALT_INTERVAL = "DUPLICATE_REQUEST_HALT_INTERVAL";

    public static final String BALANCE_LOAD_USERNAME = "BALANCE_LOAD_USERNAME";

    public static final String BALANCE_LOAD_PASSWORD = "BALANCE_LOAD_PASSWORD";

    public static final String WALLET_PAYMENT_URL = "WALLET_PAYMENT_URL";

    public static final String WALLET_PAYMENT_USERNAME = "WALLET_PAYMENT_USERNAME";

    public static final String WALLET_PAYMENT_PASSWORD = "WALLET_PAYMENT_PASSWORD";

    public static final String WALLET_PAYMENT_CONNECT_TIMEOUT = "WALLET_PAYMENT_CONNECT_TIMEOUT";

    public static final String WALLET_PAYMENT_READ_TIMEOUT = "WALLET_PAYMENT_READ_TIMEOUT";

    public static final String WALLET_VALIDATION_URL = "WALLET_VALIDATION_URL";

    public static final String WALLET_VALIDATION_USERNAME = "WALLET_VALIDATION_USERNAME";

    public static final String WALLET_VALIDATION_PASSWORD = "WALLET_VALIDATION_PASSWORD";

    public static final String WALLET_VALIDATION_CONNECT_TIMEOUT = "WALLET_VALIDATION_CONNECT_TIMEOUT";

    public static final String WALLET_VALIDATION_READ_TIMEOUT = "WALLET_VALIDATION_READ_TIMEOUT";

    public static final String SUPER_CLIENTS = "SUPER_CLIENTS";

    public static final String APP_INSTANCE = "APP_INSTANCE";

    public static final String IMAGE_BASE_URL = "IMAGE_BASE_URL";

    public static final String ESEWA_CALLBACK_USERNAME = "ESEWA_CALLBACK_USERNAME";

    public static final String ESEWA_CALLBACK_PASSWORD = "ESEWA_CALLBACK_PASSWORD";

    public static final String ESEWA_CALLBACK_URL = "ESEWA_CALLBACK_URL";

    public static final String ESEWA_CALLBACK_CONNECT_TIMEOUT = "ESEWA_CALLBACK_CONNECT_TIMEOUT";

    public static final String ESEWA_CALLBACK_READ_TIMEOUT = "ESEWA_CALLBACK_READ_TIMEOUT";

    public static final String CALLBACK_ON_SYNC = "CALLBACK_ON_SYNC";
}
