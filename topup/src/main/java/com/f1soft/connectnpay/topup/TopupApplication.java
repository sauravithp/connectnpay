package com.f1soft.connectnpay.topup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Properties;

@SpringBootApplication(scanBasePackages = {"com.f1soft.connectnpay.topup"})
public class TopupApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TopupApplication.class)
				 .properties(getProperties());
	}

	static Properties getProperties() {
		Properties props = new Properties();
		props.put("spring.config.location", PropertyNames.APP);
		return props;
	}

	public static void main(String[] args) {
		new SpringApplicationBuilder(TopupApplication.class)
				.sources(TopupApplication.class)
				.properties(getProperties())
				.run(args);
	}

}
