package com.f1soft.connectnpay.topup.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 * @author santosh
 */
public class Formater {

    public static final String DECIMAL_POINT_FORMAT = "0.00";
    public static final String AMOUNT_FORMAT = "0.##";

    public static String formatAmount(Double amount, String format) {

        DecimalFormat df = new DecimalFormat(format);
        String formatedAmount = df.format(amount);

        return formatedAmount;
    }

    public static BigDecimal doubleToBigDecimal(Double amount) {
        return new BigDecimal(String.valueOf(amount));
    }
}
