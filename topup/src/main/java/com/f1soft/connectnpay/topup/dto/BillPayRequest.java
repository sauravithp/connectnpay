package com.f1soft.connectnpay.topup.dto;


import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 *
 * @author Manjit Shakya <manjit.shakya@f1soft.com>
 */
@Getter
@Setter
public class BillPayRequest extends ModelBase{
    
    private String servicecode;
    private String requestid;
    private String agenttransid;
    private String mobileno;
    private String walletid;
    private BigDecimal amount;
    private String clientusername;
    private String password;
    private String channel;
    private String customerid;
    private String type;
    
}
