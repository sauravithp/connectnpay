package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */

@Getter
@Setter
public class TransactionStatusDTO extends ModelBase{
    private String clientusername;
    private String password;
    private List<CheckStatusTransaction> transactions;
}
