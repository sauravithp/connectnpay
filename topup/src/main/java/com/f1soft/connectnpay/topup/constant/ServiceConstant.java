package com.f1soft.connectnpay.topup.constant;

/**
 *
 * @author santosh
 */
public class ServiceConstant {

    /*
     * Service vendor code constant
     */
    public static final String NTC = "NTC";
    public static final String TELCO_NTC = "TELCO_NTC";
    public static final String NCELL = "NCELL";
    public static final String PAYPOINT_NCELL = "PAYPOINT_NCELL";
    public static final String DISHHOME = "DISHHOME";
    public static final String DISHHOME_RECHARGE = "DISHHOME_RECHARGE";
    public static final String SIMTV = "SIMTV";
    public static final String TELCO_NCELL = "TELCO_NCELL";
    public static final String SIMTV_REVISED = "SIMTV_REVISED";
    public static final String SMART_CELL = "SMART_CELL";
    public static final String SUBISU = "SUBISU";
    public static final String NCELL_DATA = "NCELL_DATA";
    public static final String NT_DATA = "NT_DATA";
    public static final String REQUEST_TYPE_LIC_POLICY_INQUIRY = "LIC_POLICY_INQUIRY";
    /*
     * Default service default parameter constant
     */
    public static final String SERVICE_USERNAME = "username";
    public static final String SERVICE_PASSWORD = "password";
    public static final String ACTOR_ID = "actorId";
    public static final String NOTIFY_AGENT = "notifyAgent";
    public static final String PRODUCT_ID = "productId";
    public static final String PAYMENT_MODE = "paymentMode";
    public static final long NTC_AMOUNT_MULTIPLIER = 100000;
    public static final long AMOUNT_MULTIPLIER = 100;
    public static final String TOKEN = "token";
    public static final String X_MEDIA_KEY = "X-Media-Key";
    //smart cell
    public static final String PARTY_CODE = "partyCode";
    //simtv config
    public static final String HEADER_KEY = "headerKey";
    public static final String HEADER_VALUE = "headerValue";
    /*
     * service code constant
     */
    public static final String NCELL_TOPUP_CODE = "NP-RTP-NCELL";
    public static final String NTC_PREPAID_TOPUP_CODE = "NP-RTP-NTPRE";
    public static final String NTC_POSTPAID_TOPUP_CODE = "NP-RTP-NTPOST";
    public static final String NTC_PSTN_TOPUP_CODE = "NP-RTP-NTPSTN";
    public static final String NTC_ADSL_UL_TOPUP_CODE = "NP-RTP-NTADSL-UL";
    public static final String NTC_ADSL_VB_TOPUP_CODE = "NP-RTP-NTADSL-VB";
    public static final String DISHHOME_TOPUP_CODE = "NP-RTP-DISH";
    /*
     * service payment url request type
     */
    public static final String REQUEST_TYPE_PAYMENT = "PAYMENT";
    public static final String REQUEST_TYPE_ADVANCE_PAYMENT = "ADVANCE_PAYMENT";
    public static final String REQUEST_TYPE_CHECK_STATUS = "CHECK_STATUS";
    public static final String REQUEST_TYPE_BALANCE_INQUIRY = "BALANCE_INQUIRY";
    public static final String REQUEST_TYPE_BILL_INQUIRY = "BILL_INQUIRY";
    public static final String REQUEST_TYPE_COUNTER_LIST_QUERY = "COUNTER_QUERY";
    public static final String REQUEST_TYPE_SERVICE_CHARGE_QUERY = "SERVICE_CHARGE";
    public static final String REQUEST_TYPE_CUSTOMER_INFO = "CUSTOMER_INFO";
    public static final String REQUEST_TYPE_LOGIN = "LOGIN";
    public static final String REQUEST_TYPE_TOPUP = "TOPUP";
    public static final String REQUEST_TYPE_PAYMENT_VERIFY = "PAYMENT_VERIFY";

    /*
     * Url call order
     */
    public static final String URL_CALL_BEFORE = "BEFORE";
    public static final String URL_CALL_AFTER = "AFTER";
    /*
     * NTC response code
     */
    public static final String NTC_TIMEOUT_CODE = "12";
    public static final String NTC_TIMEOUT_CODE2 = "18";
    /*
     * Dishhome response code
     */
    public static final String DISHHOME_PAYMENT_SUBMIT = "00";
    public static final String DISHHOME_PAYMENT_SUCCESS = "13";
    public static final int DISHHOME_PAYMENT_SUCCESS_CODE = 200;
    /*
     * Ncell response code
     */
 /*
     * Ncell response sql code constant
     */
    public static final String NCELL_PAY_SUCCESS_SQL_CODE = "0";
    public static final String NCELL_PAYMENT_PRINCIPAL_SIDE_PROBLEM_CODE1 = "5";
    public static final String NCELL_PAYMENT_PRINCIPAL_SIDE_PROBLEM_CODE2 = "18";
    public static final String NCELL_PAYMENT_PRINCIPAL_SIDE_PROBLEM_CODE3 = "100";
    /*
     * Ncell operation state constant
     */
    public static final Integer NCELL_PAY_SUCCESS_STATE = 60;
    public static final Integer NCELL_PAY_NOT_CONFIRMED_STATE1 = 0;
    public static final Integer NCELL_PAY_NOT_CONFIRMED_STATE2 = 20;
    public static final Integer NCELL_PAY_NOT_CONFIRMED_STATE3 = 40;
    public static final Integer NCELL_PAY_ERROR_STATE = 80;
    public static final Integer NCELL_PAY_REJECTED_STATE = -2;
    /*
     * SimTv Topup response status
     */
    public static final String SIM_TV_TOPUP_SUCCESS = "OK";
    public static final String SIM_TV_TOPUP_FAILURE = "Error";
    public static final String SIM_TV_REVISED_TOPUP_SUCCESS_STATUS = "success";
    public static final String SIM_TV_REVISED_TOPUP_FAILURE_STATUS = "error";
    public static final String SIM_TV_REVISED_SUCCESS_CODE = "200";
    /*
     * Service scheme code constant
     */
    public static final Character SCHEME_TYPE_CHARGE = 'C';
    public static final Character SCHEME_TYPE_DISCOUNT = 'D';
    public static final Character SCHEME_TYPE_NONE = 'N';
    public static final String DISHHOME_LOGIN_FAILURE = "LF";
    /*
     * Payment response code
     */
    public static final String API_TXN_SUCCESS = "00";
    public static final String API_TXN_FAILURE = "01";
    public static final String API_TXN_TIMEOUT = "02";
//    public static final String API_TXN_SUSPICIOUS = "03";
    public static final String API_TXN_NOT_CONFIRMED = "04";
//    public static final String API_TXN_TO_REPROCESS = "05";
    public static final String TXN_FAILURE = "06";
    /*
     * service payment for
     */
    public static final String PRIMARY_REQUEST = "PRIMARY";
    public static final String SECONDARY_REQUEST = "SECONDARY";
    /*
     * dishhome identifier
     */
    public static final String DISHHOME_CHIPID = "chip";
    public static final String DISHHOME_ACCOUNTID = "account";
    /*
     * Ncell payment mode value
     */
    public static final String PAYMENT_MODE_OFFLINE = "OFFLINE";
    public static final String PAYMENT_MODE_ONLINE = "ONINE";
    /*
     * PayPoint service constants
     */
    public static final String PAYPOINT_COMPANY_CODE = "companyCode";
    public static final String PAYPOINT_SALE_POINT_TYPE = "salePointType";
    public static final String PAYPOINT_NCELL_SERVICE_CODE = "ncellServiceCode";
    public static final String PAYPOINT_SUCCESS_CODE = "000";
    public static final String PAYPOINT_TIMEOUT_CODE = "099";
    public static final long PAYPOINT_AMOUNT_MULTIPLIER = 100;
    /*
     * Ncell direct payment service constants
     */
    public static final String NCELL_DIRECT_TELNET_HOST = "host";
    public static final String NCELL_DIRECT_TELNET_PORT = "port";
    public static final String NCELL_DIRECT_REQUEST_TYPE = "requesttype";
    public static final String NCELL_DIRECT_AGENT_CODE = "agentcode";
    public static final String NCELL_DIRECT_PIN = "pin";
    public static final String NCELL_DIRECT_AGENT_TRANS_ID = "agenttransid";
    public static final String NCELL_DIRECT_VENDOR_CODE = "vendorcode";
    public static final String NCELL_DIRECT_PRODUCT_CODE = "productcode";
    public static final String NCELL_DIRECT_CLIENT_TYPE = "clienttype";
    public static final String NCELL_DIRECT_COMMENTS = "comments";
    public static final String NCELL_DIRECT_SUCCESS_CODE = "0";
    public static final String NCELL_DIRECT_TIMEOUT_CODE = "903";
    public static final String NCELL_DIRECT_TOPUP_RESPONSE_PREFIX = "<estel><header><responsetype>TOPUP</responsetype>";
    /*
     * Smart cell response codes
     */
    public static final String SMART_CELL_TOPUP_SUCCESS = "0";
    public static final Integer DISHHOME_CASID_VERIFICATION_SUCCESS_CODE = 200;

    public interface NtcServiceCodes {

        public static final String NTC_PREPAID_TOPUP_CODE = "NP-RTP-CNTPRE";
        public static final String NTC_POSTPAID_TOPUP_CODE = "NP-RTP-CNTPOST";
        public static final String NTC_PSTN_TOPUP_CODE = "NP-RTP-CNTPSTN";
        public static final String NTC_ADSL_UL_TOPUP_CODE = "NP-RTP-CNTADSL-UL";
        public static final String NTC_ADSL_VB_TOPUP_CODE = "NP-RTP-CNTADSL-VB";
        public static final String NTC_WIMAX_TOPUP_CODE = "NP-RTP-NTWIMAX";
        public static final String NTC_FTTH_TOPUP_CODE = "NP-RTP-NTFTTH";
    }

    public interface MposStatusCodes {

        public static final String NON_EXISTED_NUMBER = "96";
        public static final String TOPUP_TIMEOUT = "97";
        public static final String NO_BALANCE = "98";
        public static final String FAILURE_CODE = "99";
        String SUCC_TXN_CODE = "1";
        String CANCEL_TXN_CODE = "0";
    }

    public static final String NEA_VENDOR_CODE = "NEA";
    public static final String HAMRO_TV_VENDOR_CODE = "HAMROTV";
    public static final String INSURANCE_VENDOR_CODE = "INSURANCE";
    public static final String KHALTI_KHANEPANI_VENDOR_CODE = "KHALTI_KHANEPANI";
    public static final String SOFTLAB_KHANEPANI_VENDOR_CODE = "SOFTLAB_KHANEPANI";
    public static final String WATERMARK_KHANEPANI_VENDOR_CODE = "WATERMARK_KHANEPANI";
    public static final String MEROTV_VENDOR_CODE = "MEROTV";
    public static final String DISHHOME_VENODR_CODE = "DISHHOME";
    public static final String DISHHOME_RECHARGE_VENODR_CODE = "DISHHOME_RECHARGE";
    public static final String CONNECTPLUS_SCHOOL_PAYMENT_VENDOR_CODE = "CPLUSSCHOOL";
    public static final String VIANET_VENDOR_CODE = "VIANET";
    public static final String CLASSICTECH_VENDOR_CODE = "CLASSICTECH";
    public static final String SUBISU_VENDOR_CODE = "SUBISU";
    public static final String SKYTV_VENDOR_CODE = "SKYTV";
    public static final String WEBSURFER_VENDOR_CODE = "WEBSURFER";
    public static final String WORLDLINK_VENDOR_CODE = "WORLDLINK";
    public static final String KHALTI_KHANEPANI_SERVICE_CODE = "NP-RTP-KHALTI-KHANEPANI";
    public static final String SOFTLAB_KHANEPANI_SERVICE_CODE = "NP-RTP-SOFTLAB-KHANEPANI";
    public static final String WATERMARK_KHANEPANI_SERVICE_CODE = "NP-RTP-WATERMARK-KHANEPANI";
    public static final String SUBISU_SERVICE_CODE = "NP-RTP-SUBISU";
    public static final String MEROTV_SERVICE_CODE = "NP-RTP-MEROTV";
    public static final String VIANET_SERVICE_CODE = "NP-RTP-VIANET";
    public static final String CLASSICTECH_SERVICE_CODE = "NP-RTP-CLASSICTECH";
    public static final String CONNECT_PLUS_SCHOOL_SERVICE_CODE = "NP-RTP-CPLUSSCHOOL";

    public static final String NEPAL_LIFE_INSURANCE_SERVICE_CODE = "NP-RTP-NLIC";
    public static final String JYOTI_LIFE_INSURANCE_SERVICE_CODE = "NP-RTP-JLIC";
    public static final String NATIONAL_LIFE_INSURANCE_SERVICE_CODE = "NP-RTP-NATIONAL-INSURANCE";
    public static final String SKYTV_SERVICE_CODE = "NP-RTP-SKYTV";
    public static final String SKYTV_INTERNET_SERVICE_CODE = "NP-RTP-INTERNET-SKYTV";
    public static final String SKYTV_DIGITALTV_SERVICE_CODE = "NP-RTP-DIGITALTV-SKYTV";
    public static final String WORLDLINK_SERVICE_CODE = "NP-RTP-WORLDLINK";
    public static final String WORLDLINK_ESEWA_SERVICE_CODE = "NP-RTP-ESWORLDLINK";

    public static final String WEBSURFER_INTERNET_SERVICE_CODE = "NP-RTP-INTERNET-WEBSURFER";
    public static final String WEBSURFER_SERVICE_CODE = "NP-RTP-WEBSURFER";

    public static final String SAGARMATHA_INSURANCE_SERVICE_CODE = "NP-RTP-SLIC";
    public static final String SAGARMATHA_INSURANCE_VENDOR_CODE = "SAGARMATHA_INSURANCE";

    public static final String JYOTI_SIDDHILIFE_INSURANCE_SERVICE_CODE = "NP-RTP-JYOTI-INSURANCE";
    public static final String JYOTI_INSURANCE_VENDOR_CODE = "JYOTI_INSURANCE";

    public static final String NETTV_PURCHASE_SERVICE_CODE = "NP-RTP-NETTV-PURCHASE";
    public static final String NETTV_TOPUP_SERVICE_CODE = "NP-RTP-NETTV-TOPUP";
    public static final String NETTV_VENDOR_CODE = "NETTV";

    public static final String ESEWA_VENDOR_CODE = "ESEWA";
    public static final String ESEWA_LOAD_SERVICE_CODE = "NP-RTP-ESEWALOAD";

    public static final String BILL_INQUIRY_REQUEST_STAGE = "BILL_INQUIRY_REQUEST";
    public static final String BILL_INQUIRY_STAGE = "BILL_INQUIRY";
    public static final String BILL_PAYMENT_STAGE = "BILL_PAYMENT";

    public static final String MERCHANT_NAME = "merchantName";
    public static final String API_KEY = "apiKey";

    public static final String DARSANTECH_INSURANCE_SERVICE_CODE = "NP-RTP-DLIC";
    public static final String DARSANTECH_INSURANCE_VENDOR_CODE = "DARSANTECH_INSURANCE";

    public static final String WORLDLINK_ESEWA_VENDOR_CODE = "WORLDLINK_ESEWA";

    public static interface StatusChannel {

        public static final String WORLDLINK_ESEWA_CHANNEL = "esewa";
        public static final String WORLDLINK_FONEPAY_CHANNEL = "fonepay";
    }

    public static final String BIG_MOVIES_SERVICE_CODE = "NP-RTP-BIGMOVIES";
    public static final String BIG_MOVIES_VENDOR_CODE = "BIGMOVIES";

    public static final String BUSSEWA_SERVICE_CODE = "NP-RTP-BUSSEWA";
    public static final String BUSSEWA_VENDOR_CODE = "BUSSEWA";

    public static final String MANAKAMANA_SERVICE_CODE = "NP-RTP-MANAKAMANA";
    public static final String MANAKAMANA_VENDOR_CODE = "MANAKAMANA_CABLE_CAR";

    public static final String SHANGRILA_SERVICE_CODE = "NP-RTP-SHANGRILA";
    public static final String SHANGRILA_VENDOR_CODE = "SHANGRILA";

    public static final String NEPALLIFE_SERVICE_CODE = "NP-RTP-NEPALLIFE";
    public static final String NEPALLIFE_VENDOR_CODE = "NEPALLIFE";

    public static final String PLAZMA_KHANEPANI_SERVICE_CODE = "NP-RTP-PLAZMA-KHANEPANI";
    public static final String PLAZMA_KHANEPANI_VENDOR_CODE = "PLAZMA_KHANEPANI";

    public static final String SANIMA_LIFE_INSURANCE_SERVICE_CODE = "NP-RTP-SANLIC";
    public static final String SANIMA_LIFE_INSURANCE_VENDOR_CODE = "SANIMA_INSURANCE";

    public static final String NIBL_ACE_SERVICE_CODE = "NP-RTP-NIBL-ACE";
    public static final String NIBL_ACE_VENDOR_CODE = "NIBLACE";

    public static final String MAXTV_SERVICE_CODE = "NP-RTP-MAXTV";
    public static final String MAXTV_VENDOR_CODE = "MAXTV";

    public static final String NCELL_DATA_SERVICE_CODE = "NP-RTP-NCELL-DATA";

    public static final String PRIME_LIFE_INSURANCE_SERVICE_CODE = "NP-RTP-PRILIC";
    public static final String PRIME_LIFE_INSURANCE_VENDOR_CODE = "PRIME_INSURANCE";

    public static final String MIDAS_SERVICE_CODE = "NP-RTP-MIDAS";
    public static final String MIDAS_VENDOR_CODE = "MIDAS";

    public static final String PRABHUTV_SERVICE_CODE = "NP-RTP-PRABHUTV";
    public static final String PRABHUTV_VENDOR_CODE = "PRABHUTV";

    public static final String BARAHI_SERVICE_CODE = "NP-RTP-BARAHI";
    public static final String BARAHI_VENDOR_CODE = "BARAHI";

    public static final String NLG_INSURANCE_SERVICE_CODE = "NP-RTP-NLG";
    public static final String NLG_INSURANCE_VENDOR_CODE = "NLG_INSURANCE";

    public static final String HEARTSUN_KHANEPANI_VENDOR_CODE = "HEARTSUN_KHANEPANI";
    public static final String HEARTSUN_KHANEPANI_SERVICE_CODE = "NP-RTP-HEARTSUN-KHANEPANI";

    public static final String ASINKA_SERVICE_CODE = "NP-RTP-ASINKA";
    public static final String ASINKA_VENDOR_CODE = "ASINKA";

    public static final String PALSNET_SERVICE_CODE = "NP-RTP-PALSNET";
    public static final String PALSNET_VENDOR_CODE = "PALSNET";

    public static final String BROADLINK_SERVICE_CODE = "NP-RTP-BROADLINK";
    public static final String BROADLINK_VENDOR_CODE = "BROADLINK";

    public static final String ULTRANET_SERVICE_CODE = "NP-RTP-ULTRANET";
    public static final String ULTRANET_VENDOR_CODE = "ULTRANET";

    public static final String LOOPNET_SERVICE_CODE = "NP-RTP-LOOPNET";
    public static final String LOOPNET_VENDOR_CODE = "LOOPNET";

    public static final String MEROSHARE_SERVICE_CODE = "NP-RTP-MEROSHARE";
    public static final String MEROSHARE_VENDOR_CODE = "MEROSHARE";

    public static final String WATERMARK_ELECTRICITY_VENDOR_CODE = "WATERMARK_ELECTRICITY";
    public static final String WATERMARK_ELECTRICITY_SERVICE_CODE = "NP-RTP-WATERMARK-ELECTRICITY";

    public static final String WATERMARK_APARTMENT_SERVICE_CODE = "NP-RTP-WATERMARK-APARTMENT";
    public static final String WATERMARK_APARTMENT_VENDOR_CODE = "WATERMARK_APARTMENT";

    public static final String VERIFICATION_USERNAME = "verificationUsername";
    public static final String VERIFICATION_PASSWORD = "verificationPassword";

    public static final String TECHMIND_SERVICE_CODE = "NP-RTP-TECHMIND";
    public static final String TECHMIND_VENDOR_CODE = "TECHMIND";

    public static final String FIRSTLINK_SERVICE_CODE = "NP-RTP-FIRSTLINK";
    public static final String FIRSTLINK_VENDOR_CODE = "FIRSTLINK";

    public static final String IME_LIFE_INSURANCE_SERVICE_CODE = "NP-RTP-IMELIC";
    public static final String IME_LIFE_INSURANCE_VENDOR_CODE = "IME_LIFE_INSURANCE";

    public static final String ARHANT_INSURANCE_SERVICE_CODE = "NP-RTP-ARHANT";
    public static final String ARHANT_INSURANCE_VENDOR_CODE = "ARHANT_INSURANCE";

    public static final String NEPALLIFE_LOAN_SERVICE_CODE = "NP-RTP-NEPALLIFE-LOAN";
    public static final String NEPALLIFE_LOAN_VENDOR_CODE = "NEPALLIFE-LOAN";

    public static final String NTC_DATA_SERVICE_CODE = "NP-RTP-NTC-DATA";

    public static final String ECHALAN_SERVICE_CODE = "NP-RTP-ECHALAN";
    public static final String ECHALAN_VENDOR_CODE = "ECHALAN";

}
