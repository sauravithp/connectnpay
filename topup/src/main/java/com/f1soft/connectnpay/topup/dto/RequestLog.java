package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Getter
@Setter
@Entity
@Table(name = "REQUEST_LOG")
@NamedQueries({
    @NamedQuery(name = "RequestLog.findAll", query = "SELECT b FROM RequestLog b"),
    @NamedQuery(name = "RequestLog.findById", query = "SELECT b FROM RequestLog b WHERE b.id = :id")
})
public class RequestLog extends DomainBase {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Column(name = "SERVICE_CODE", length = 50)
    private String serviceCode;
    @Column(name = "REQUEST_ID", length = 50)
    private String requestId;
    @Column(name = "CLIENT_TRACE_ID", length = 50)
    private String clientTraceId;
    @Column(name = "CUSTOMER_REF_ID", length = 50)
    private String customerRefId;
    @Column(name = "MOBILE_NUMBER", length = 50)
    private String mobileNumber;
    @Column(name = "AMOUNT", length = 50)
    private String amount;
    @Column(name = "CLIENT_USERNAME", length = 50)
    private String clientUsername;
    @Column(name = "PASSWORD", length = 50)
    private String password;
    @Column(name = "CHANNEL", length = 50)
    private String channel;
    @Column(name = "RESPONSE_CODE", length = 10)
    private String responseCode;
    @Column(name = "RESPONSE_DESC", length = 200)
    private String responseDesc;
    @Basic(optional = false)
    @Column(name = "RECORDED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date recordedDate;
    @Column(name = "TYPE", length = 30)
    private String type;
    @Column(name = "BANK_CODE", length = 20)
    private String bankCode;
}
