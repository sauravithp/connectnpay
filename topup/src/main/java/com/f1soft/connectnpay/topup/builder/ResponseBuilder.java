package com.f1soft.connectnpay.topup.builder;

import com.f1soft.connectnpay.topup.constant.ServiceConstant;
import com.f1soft.connectnpay.topup.dto.RequestLog;
import com.f1soft.connectnpay.topup.dto.RequestLogDTO;
import com.f1soft.connectnpay.topup.dto.ServerResponse;
import com.f1soft.connectnpay.topup.model.ServiceResponseInfo;
import com.f1soft.connectnpay.topup.util.ResourceLoader;
import lombok.extern.slf4j.Slf4j;

import static com.f1soft.connectnpay.topup.constant.RequestResponseCode.PAYMENT_SUCCESSFUL;
import static com.f1soft.connectnpay.topup.constant.RequestResponseCode.SERVER_CONNECTION_TIMEOUT;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Slf4j
public class ResponseBuilder {
    /*
     * Successful response obtained from vendor
     */
    public static ServerResponse buildVendorSuccessMessage(ServerResponse serverResponse) {
        ServiceResponseInfo serviceResponseInfo = ResourceLoader.responseMap.get(PAYMENT_SUCCESSFUL);
        serverResponse.setRespCode(serviceResponseInfo.getResponseCode());
        serverResponse.setMessage(serviceResponseInfo.getResponseDesc());
        serverResponse.setSuccess(true);
        //txnCode is directly related to client request status flag (should be handled carefully)
        serverResponse.setTxnCode(ServiceConstant.API_TXN_SUCCESS);
        return serverResponse;
    }

    /*
     * Failed response obtained from vendor
     */
    public static ServerResponse buildVendorFailureMessage(ServerResponse serverResponse, String messageCode) {
        ServiceResponseInfo serviceResponseInfo = ResourceLoader.responseMap.get(messageCode);
        serverResponse.setRespCode(serviceResponseInfo.getResponseCode());
        serverResponse.setMessage(serviceResponseInfo.getResponseDesc());
        serverResponse.setSuccess(false);
        //txnCode is directly related to client request status flag (should be handled carefully)
        serverResponse.setTxnCode(ServiceConstant.API_TXN_FAILURE);
        return serverResponse;
    }

    /*
     * For timeout cases
     */
    public static ServerResponse buildTimeoutMessage(ServerResponse serverResponse) {
        ServiceResponseInfo serviceResponseInfo = ResourceLoader.responseMap.get(SERVER_CONNECTION_TIMEOUT);
        serverResponse.setRespCode(serviceResponseInfo.getResponseCode());
        serverResponse.setMessage(serviceResponseInfo.getResponseDesc());
        serverResponse.setSuccess(false);
        //txnCode is directly related to client request status flag (should be handled carefully)
        serverResponse.setTxnCode(ServiceConstant.API_TXN_TIMEOUT);
        return serverResponse;
    }

    /*
     * For validation failure and local wallet transaction failure case
     */
    public static ServerResponse buildFailureMessage(ServerResponse serverResponse, String messageCode) {
        ServiceResponseInfo serviceResponseInfo = ResourceLoader.responseMap.get(messageCode);
        serverResponse.setRespCode(serviceResponseInfo.getResponseCode());
        serverResponse.setMessage(serviceResponseInfo.getResponseDesc());
        serverResponse.setSuccess(false);
        //txnCode is directly related to client request status flag (should be handled carefully)
        //local failure txn code is used for local validation failure or transaction failure
        serverResponse.setTxnCode(ServiceConstant.TXN_FAILURE);
        return serverResponse;
    }

    public static RequestLogDTO setResponseLog(RequestLogDTO requestLog, String responseCode, String responseDesc) {
        requestLog.setResponseCode(responseCode);
        requestLog.setResponseDesc(responseDesc);
        return requestLog;
    }

    public static ServerResponse buildSuccessMessage(ServerResponse serverResponse, String messageCode) {
        ServiceResponseInfo serviceResponseInfo = ResourceLoader.responseMap.get(messageCode);
        serverResponse.setRespCode(serviceResponseInfo.getResponseCode());
        serverResponse.setMessage(serviceResponseInfo.getResponseDesc());
        serverResponse.setSuccess(true);
        //txnCode is directly related to client request status flag (should be handled carefully)
        serverResponse.setTxnCode(ServiceConstant.API_TXN_SUCCESS);
        return serverResponse;
    }

    public static ServerResponse buildFailureMessage(String respCode, String message) {
        ServerResponse serverResponse = new ServerResponse();
        serverResponse.setRespCode(respCode);
        serverResponse.setMessage(message);
        serverResponse.setSuccess(false);
        //txnCode is directly related to client request status flag (should be handled carefully)
        //local failure txn code is used for local validation failure or transaction failure
        serverResponse.setTxnCode(ServiceConstant.TXN_FAILURE);
        return serverResponse;
    }

    public static ServerResponse buildRedisResponse(ServerResponse response) {
        ServerResponse serverResponse = new ServerResponse();

        if (response.isSuccess()) {
            serverResponse.setSuccess(true);
            serverResponse.setRespCode("0");
            serverResponse.setMessage("Cache cleared successfully.");
        } else {
            serverResponse.setSuccess(false);
            serverResponse.setRespCode("-6");
            serverResponse.setMessage(response.getMessage());
        }
        return serverResponse;
    }

    public static ServerResponse buildEhcacheSuccessMessage(ServerResponse serverResponse, String message) {
        serverResponse.setRespCode("0");
        serverResponse.setMessage(message);
        return serverResponse;
    }

    public static ServerResponse buildBookFailureResponse(String responseCode, String respDesc) {
        ServerResponse response = new ServerResponse();
        response.setSuccess(false);
        response.setMessage(respDesc);
        response.setRespCode(responseCode);
        return response;
    }

    public static ServerResponse buildVendorFailureResponse(String rawResponse, String responseCode, String respDesc) {
        ServerResponse response = new ServerResponse();
        response.setSuccess(false);
        response.setTxnCode(ServiceConstant.API_TXN_FAILURE);
        response.setVendorRespCode(responseCode);
        response.setVendorRespDesc(respDesc);
        response.setMessage(respDesc);
        response.setRespCode(responseCode);
        response.setVendorResponseObtained(true);
        response.setRawResponse(rawResponse);
        return response;
    }

    public static ServerResponse buildFailureResponse(ServerResponse response, String message) {
        response.setSuccess(false);
        response.setMessage(message);
        response.setRespCode("1");
        return response;
    }

    public static ServerResponse processErrorData(String requestId,
                                                  RequestLog requestLog,
                                                  ServerResponse serverResponse,
                                                  String errorType) {
//                                                  RequestLogHandler logHandler) {
        log.debug("Request is invalid to process for request id : " + requestId);
//        logHandler.updateLog(new RequestLogEvent(requestLog, serverResponse.getRespCode(), serverResponse.getMessage()));
        log.debug("========== Card Server final response (Upon " + errorType + ") ==============");
        log.debug("request id : " + requestId);
        log.debug("result Code : " + serverResponse.getRespCode());
        log.debug("result description : " + serverResponse.getMessage());
        log.debug("=====================================================");
        return serverResponse;
    }
}
