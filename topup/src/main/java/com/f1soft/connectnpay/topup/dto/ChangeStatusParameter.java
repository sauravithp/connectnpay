package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeStatusParameter extends ModelBase {

    private Long clientRequestId;
    private Integer status;
    private String vendorTraceId;
    private String vendorResponseCode;
    private String vendorResponseDesc;
    private Long initiatorId;
    private String remarks;
}
