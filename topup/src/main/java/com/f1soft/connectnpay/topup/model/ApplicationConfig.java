package com.f1soft.connectnpay.topup.model;

import com.f1soft.connectnpay.topup.dto.DomainBase;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;

/**
 *
 * @author santosh
 */
@Getter
@Setter
@Entity
@Table(name = "APPLICATION_CONFIG")
@NamedQueries({
    @NamedQuery(name = "ApplicationConfig.findAll", query = "SELECT t FROM ApplicationConfig t")
    ,
    @NamedQuery(name = "ApplicationConfig.findById", query = "SELECT t FROM ApplicationConfig t where t.id = :id")
    ,
    @NamedQuery(name = "ApplicationConfig.findByConfigKey", query = "SELECT t FROM ApplicationConfig t where t.configKey = :configKey")
})
public class ApplicationConfig extends DomainBase {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "CONFIG_KEY", nullable = false, length = 50)
    private String configKey;
    @Column(name = "CONFIG_VALUE", length = 500)
    private String configValue;

    public ApplicationConfig() {
    }

    public ApplicationConfig(Long id) {
        this.id = id;
    }

}
