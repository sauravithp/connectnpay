package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "SERVICE_RESPONSE_INFO", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"REF_CODE"})})
@NamedQueries({
    @NamedQuery(name = "ServiceResponseInfo.findAll", query = "SELECT b FROM ServiceResponseInfo b")
})
public class ServiceResponseInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Column(name = "RESPONSE_CODE", length = 5)
    private String responseCode;
    @Column(name = "RESPONSE_DESC")
    private String responseDesc;
    @Column(name = "REF_CODE")
    private String refCode;
    @Column(name = "RESPONSE_TYPE")
    private String responseType;

    public ServiceResponseInfo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
