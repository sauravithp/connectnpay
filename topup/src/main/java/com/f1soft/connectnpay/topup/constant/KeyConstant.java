package com.f1soft.connectnpay.topup.constant;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public interface KeyConstant {
    /**
     * Dont forget to added the option to CacheCleaner
     */
    String GLUE = ":";
    String CLIENT = ClearCacheConstant.CLIENT + GLUE;
    String SERVICE = ClearCacheConstant.SERVICE + GLUE;
    String COUNTER = ClearCacheConstant.COUNTER + GLUE;

    String ALL = "ALL";

    String SINGLE = "SINGLE";
    String ALL_GLUE = ALL + GLUE;
    String SINGLE_GLUE = SINGLE + GLUE;

    String VENDOR_CODE = "VENDOR_CODE" + GLUE;

    String COUNTERS = "COUNTERS";
    String RSA_KEY = "RSA_KEY" + GLUE;
    String DATA = "DATA" + GLUE;
    String SYSTEM_CREDENTIAL = "SYSTEM_CREDENTIAL" + GLUE;

    public static interface CounterKey {

        String NEA_COUNTERS = "NEA_COUNTERS";
        String KHANEPANI_COUNTERS = "KHANEPANI_COUNTERS";
        String ELECTRICITY_COUNTERS = "ELECTRICITY_COUNTERS";
        String WATERMARK_KHANEPANI_COUNTERS = "WATERMARK_KHANEPANI_COUNTERS";
        String H2O_KHANEPANI_COUNTERS = "H2O_KHANEPANI_COUNTERS";
        String PLAZMA_KHANEPANI_COUNTERS = "PLAZMA_KHANEPANI_COUNTERS";
        String HEARTSUN_KHANEPANI_COUNTERS = "HEARTSUN_KHANEPANI_COUNTERS";
        String BUSSEWA_ROUTES = "BUSSEWA_ROUTES";
        String WATERMARK_ELECTRICITY_COUNTERS = "WATERMARK_ELECTRICITY_COUNTERS";
        String MEROSHARE_DEPOSITARY_PARTICIPANTS = "MEROSHARE_DEPOSITARY_PARTICIPANTS";
        String APARTMENT_COUNTERS = "APARTMENT_COUNTERS";
        String WATERMARK_APARTMENT_COUNTERS = "WATERMARK_APARTMENT_COUNTERS";
        String SHANGRILA_TAX_COUNTERS = "SHANGRILA_TAX_COUNTERS";
        String DARSANTECH_INSURANCE_COUNTERS = "DARSANTECH_INSURANCE_COUNTERS";
        String ARHANT_INSURANCE_COUNTERS = "ARHANT_INSURANCE_COUNTERS";
        String INSURANCE_COUNTERS = "INSURANCE_COUNTERS";
    }

    public static interface ServiceProvider {

        String NTC = "NTC_PACKAGES";
        String NCELL = "NCELL_PACKAGES";
    }
}
