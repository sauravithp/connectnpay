package com.f1soft.connectnpay.topup.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manjit Shakya <manjit.shakya@f1soft.com>
 */
@Getter
@Setter
@NoArgsConstructor
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "service_credential")
@Table(name = "SERVICE_CREDENTIAL")
public class ServiceCredential implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Basic(optional = false)
    @Column(name = "ACTIVE")
    private char active;
    @JoinColumn(name = "CLIENT_CHANNEL_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private ClientChannel clientChannel;
    @JoinColumn(name = "SERVICE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Service service;
    @Column(name = "IS_DEFAULT")
    private char isDefault;
    @JoinColumn(name = "ADDED_BY", referencedColumnName = "ID", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser addedBy;
    @Basic(optional = false)
    @Column(name = "ADDED_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected Date addedDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    protected ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastModifiedDate;
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY, region = "service_credential_parameter")
    @OneToMany(mappedBy = "serviceCredential", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<ServiceCredentialParameter> serviceCredentialParameters;
}
