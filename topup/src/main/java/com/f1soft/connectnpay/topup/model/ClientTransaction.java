package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Entity()
@Table(name = "CLIENT_TRANSACTION", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"CLIENT_REQUEST_ID", "TXN_TYPE", "BALANCE_REQUEST_ID"})})
@NamedQueries({
    @NamedQuery(name = "ClientTransaction.findAll", query = "SELECT b FROM ClientTransaction b")
    ,
    @NamedQuery(name = "ClientTransaction.findById", query = "SELECT b FROM ClientTransaction b WHERE b.id = :id")
    ,
    @NamedQuery(name = "ClientTransaction.findByRequestIdAndTxnType", query = "SELECT b FROM ClientTransaction b WHERE b.txnType =:txnType AND b.clientRequest.id = :clientRequestId")
})
public class ClientTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Column(name = "CLIENT_ACCOUNT_ID", nullable = false, precision = 22)
    private Long clientAccountId;
    @Basic(optional = false)
    @JoinColumn(name = "CLIENT_ACCOUNT_ID", insertable = false, updatable = false)
    @ManyToOne()
    private ClientAccount clientAccount;
    @Column(name = "SRC_CUR_CODE")
    private String sourceCurrencyCode;
    @Basic(optional = false)
    @Column(name = "REQUESTED_AMOUNT", precision = 19, scale = 2)
    private BigDecimal requestedAmount;
    @Basic(optional = false)
    @Column(name = "TXN_AMOUNT", precision = 19, scale = 2)
    private BigDecimal txnAmount;
    @Column(name = "DEST_CUR_CODE")
    private String destinationCurrencyCode;
    @Column(name = "EXCHANGE_RATE")
    private String exchangeRate;
    @Basic(optional = false)
    @Column(name = "CLIENT_OPENING_BALANCE", precision = 19, scale = 2)
    private BigDecimal clientOpeningBalance;
    @Basic(optional = false)
    @Column(name = "CLIENT_CLOSING_BALANCE", precision = 19, scale = 2)
    private BigDecimal clientClosingBalance;
    @Column(name = "TXN_TYPE", length = 2)
    private String txnType;
    @Basic(optional = false)
    @Column(name = "TXN_DATE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date txnDate;
    @JoinColumn(name = "CLIENT_REQUEST_ID")
    @ManyToOne()
    private ClientRequest clientRequest;
    @JoinColumn(name = "BALANCE_REQUEST_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private BalanceRequest balanceRequest;
    @Column(name = "SCHEME_CODE")
    private Character schemeCode;
    @Column(name = "CALCULATED_IN")
    private Character calculatedIn;
    @Column(name = "CALCULATED_RATE")
    private String calculatedRate;
    @Column(name = "CLIENT_SCHEME_RATE")
    private String clientSchemeRate;
    @Column(name = "TOTAL_SCHEME_AMOUNT", precision = 19, scale = 2)
    private BigDecimal totalSchemeAmount;
    @Column(name = "CL_SCHEME_AMOUNT", precision = 19, scale = 2)
    private BigDecimal clSchemeAmount;
    @Column(name = "PR_SCHEME_AMOUNT", precision = 19, scale = 2)
    private BigDecimal prSchemeAmount;
    @Column(name = "CLIENT_OPENING_CHARGE", precision = 19, scale = 2)
    private BigDecimal clientOpeningCharge;
    @Column(name = "CLIENT_CLOSING_CHARGE", precision = 19, scale = 2)
    private BigDecimal clientClosingCharge;
    @Column(name = "CLIENT_OPENING_DISCOUNT", precision = 19, scale = 2)
    private BigDecimal clientOpeningDiscount;
    @Column(name = "CLIENT_CLOSING_DISCOUNT", precision = 19, scale = 2)
    private BigDecimal clientClosingDiscount;

    public ClientTransaction() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientAccount getClientAccount() {
        return clientAccount;
    }

    public void setClientAccount(ClientAccount clientAccount) {
        this.clientAccount = clientAccount;
    }

    public ClientRequest getClientRequest() {
        return clientRequest;
    }

    public void setClientRequest(ClientRequest clientRequest) {
        this.clientRequest = clientRequest;
    }

    public BalanceRequest getBalanceRequest() {
        return balanceRequest;
    }

    public void setBalanceRequest(BalanceRequest balanceRequest) {
        this.balanceRequest = balanceRequest;
    }

    public BigDecimal getClientClosingBalance() {
        return clientClosingBalance;
    }

    public void setClientClosingBalance(BigDecimal clientClosingBalance) {
        this.clientClosingBalance = clientClosingBalance;
    }

    public BigDecimal getClientOpeningBalance() {
        return clientOpeningBalance;
    }

    public void setClientOpeningBalance(BigDecimal clientOpeningBalance) {
        this.clientOpeningBalance = clientOpeningBalance;
    }

    public BigDecimal getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(BigDecimal txnAmount) {
        this.txnAmount = txnAmount;
    }

    public Date getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getDestinationCurrencyCode() {
        return destinationCurrencyCode;
    }

    public void setDestinationCurrencyCode(String destinationCurrencyCode) {
        this.destinationCurrencyCode = destinationCurrencyCode;
    }

    public BigDecimal getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(BigDecimal requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public String getSourceCurrencyCode() {
        return sourceCurrencyCode;
    }

    public void setSourceCurrencyCode(String sourceCurrencyCode) {
        this.sourceCurrencyCode = sourceCurrencyCode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCalculatedRate() {
        return calculatedRate;
    }

    public void setCalculatedRate(String calculatedRate) {
        this.calculatedRate = calculatedRate;
    }

    public Character getCalculatedIn() {
        return calculatedIn;
    }

    public void setCalculatedIn(Character calculatedIn) {
        this.calculatedIn = calculatedIn;
    }

    public Character getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(Character schemeCode) {
        this.schemeCode = schemeCode;
    }

    public BigDecimal getClSchemeAmount() {
        return clSchemeAmount;
    }

    public void setClSchemeAmount(BigDecimal clSchemeAmount) {
        this.clSchemeAmount = clSchemeAmount;
    }

    public BigDecimal getPrSchemeAmount() {
        return prSchemeAmount;
    }

    public void setPrSchemeAmount(BigDecimal prSchemeAmount) {
        this.prSchemeAmount = prSchemeAmount;
    }

    public BigDecimal getTotalSchemeAmount() {
        return totalSchemeAmount;
    }

    public void setTotalSchemeAmount(BigDecimal totalSchemeAmount) {
        this.totalSchemeAmount = totalSchemeAmount;
    }

    public Long getClientAccountId() {
        return clientAccountId;
    }

    public void setClientAccountId(Long clientAccountId) {
        this.clientAccountId = clientAccountId;
    }

    public String getClientSchemeRate() {
        return clientSchemeRate;
    }

    public void setClientSchemeRate(String clientSchemeRate) {
        this.clientSchemeRate = clientSchemeRate;
    }

    public BigDecimal getClientOpeningCharge() {
        return clientOpeningCharge;
    }

    public void setClientOpeningCharge(BigDecimal clientOpeningCharge) {
        this.clientOpeningCharge = clientOpeningCharge;
    }

    public BigDecimal getClientClosingCharge() {
        return clientClosingCharge;
    }

    public void setClientClosingCharge(BigDecimal clientClosingCharge) {
        this.clientClosingCharge = clientClosingCharge;
    }

    public BigDecimal getClientOpeningDiscount() {
        return clientOpeningDiscount;
    }

    public void setClientOpeningDiscount(BigDecimal clientOpeningDiscount) {
        this.clientOpeningDiscount = clientOpeningDiscount;
    }

    public BigDecimal getClientClosingDiscount() {
        return clientClosingDiscount;
    }

    public void setClientClosingDiscount(BigDecimal clientClosingDiscount) {
        this.clientClosingDiscount = clientClosingDiscount;
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ClientTransaction[id=" + id + "]";
    }
}
