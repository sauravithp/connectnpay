package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
public class CheckStatusTxnList  extends ModelBase {

    @NotNull
    private String requestId;
    @NotNull
    private double amount;
}
