package com.f1soft.connectnpay.topup.constant;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public interface ClearCacheConstant {
    /**
     * These are the parameters passed in cache clear request
     */
    String SERVICE = "SERVICE";
    String COUNTER = "COUNTERS";
    String CLIENT = "CLIENT";
    String ALL = "ALL";

}
