package com.f1soft.connectnpay.topup.configuration;

import com.f1soft.connectnpay.topup.dto.ModelBase;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
@Getter
@Setter
public class ServerConfig extends ModelBase {

    private String neaMode;
    private String telcoMode;
    private String khanepaniMode;
    private String skyTvMode;
    private String insuranceMode;
    private String meroTvMode;
    private String dishhomeInquiryMode;
    private String simtvInquiryMode;
    private String connectPlusSchoolMode;
    private String netTvMode;
    private String walletPaymentMode;
    private String esewaLoadMode;
    private String bigMoviesMode;
    private String bussewaMode;
    private String manakamanaMode;
    private String shangrilaTaxMode;
    private String capitalMode;
    private String maxTvMode;
    private String midasMode;
    private String prabhuTvMode;
    private String barahiMode;
    private String asinkaMode;
    private String electricityMode;
    private String apartmentMode;
    private String ispMode;
    private String echalanMode;
}