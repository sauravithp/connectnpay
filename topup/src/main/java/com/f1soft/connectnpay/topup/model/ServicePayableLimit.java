package com.f1soft.connectnpay.topup.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author yogesh
 */
@Getter
@Setter
@Entity
@Cacheable(true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "service_payable_limit")
@Table(name = "SERVICE_PAYABLE_LIMIT")
public class ServicePayableLimit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", precision = 22)
    private Long id;
    @Column(name = "AMOUNT_IDENTIFIER", precision = 19, scale = 2)
    private BigDecimal amountIdentifier;
    @Basic(optional = false)
    @Column(name = "MIN_AMOUNT")
    private Double minAmount;
    @Basic(optional = false)
    @Column(name = "MAX_AMOUNT")
    private Double maxAmount;
    @Basic(optional = false)
    @JoinColumn(name = "SERVICE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Service service;
    @Basic(optional = false)
    @Column(name = "ACTIVE", length = 1)
    private Character active;
    @Basic(optional = false)
    @Column(name = "HAS_AMOUNT_IDENTIFIER", length = 1)
    private Character hasAmountIdentifier;
    @Basic(optional = false)
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ApplicationUser lastModifiedBy;
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    public ServicePayableLimit() {
    }

    @Override
    public String toString() {
        return "com.f1soft.topup.server.entities.ServicePayableLimit[id=" + id + "]";
    }
}
