package com.f1soft.connectnpay.topup.constant;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class Common {

    public static final String RANGE_PATTERN_TYPE = "RANGE";
    public static final String EXPRESSION_PATTERN_TYPE = "REGULAREXPRESSION";
    public static final String ALPHA_PATTERN_TYPE = "ALPHA";
    public static final String ALPHA_NUMERIC_PATTERN_TYPE = "ALPHA_NUMERIC";
    public static final String NUMERIC_PATTERN_TYPE = "NUMERIC";
    /*
     * Client request table: TXN_STATUS
     */
    public static final Integer TXN_PROCESSING = 0;
    public static final Integer TXN_SUCCESS = 1;
    public static final Integer TXN_FAILURE = 7;
    /*
     * Client request table: STATUS
     */
    public static final Integer PROCESSING = 1; //
    public static final Integer COMPLETE = 2; //local txn and service api response success
    public static final Integer FAILURE = 3; //local txn failure
    public static final Integer PAYMENT_FAILURE = 4; //failed from service api but local txn success
    public static final Integer TIMEOUT = 5; //timeout from service api
    public static final Integer TXN_REVERSED = 6;
    public static final Integer SUSPICIOUS = 7; //local txn success but empty response from service api
    public static final Integer PAYMENT_NOT_CONFIRMED = 8; //service payment successfully submitted but payment is not successful in real time ( so need to send status check request(for epay case))
    public static final Integer PAYMENT_TO_REPROCESS = 9; // service payment not success due to problem on principal side, need to reinitiate txn not more than 5 times with an interval of 5 minutes after 2 minutes of txn initiation (for epay))
    /**
     * Txn type constant
     */
    public static final String TXN_REQUEST = "T"; //txn request
    public static final String REVERSAL_REQUEST = "R"; //reversal txn request
    public static final String BALANCE_LOAD = "BL"; //balance load request
    /**
     * Txn Error Codes
     */
    public static final String INVALID_TXN_AMOUNT = "ITA";
    public static final String INSUFFICIENT_ACCOUNT_BALANCE = "IAB";
    public static final String EXCEPTION = "ERR";
    public static final String CLIENT_ACCOUNT_NOT_FOUND = "CANF";
    public static final String INVALID_REQUEST_PARAMETER = "IRP";
    public static final String CLIENT_AUTHENTICATION_FAILURE = "CAF";
    public static final String CLIENT_BLOCKED = "CB";
    public static final String SERVICE_NOT_AVAILABLE = "SNA";
    public static final String DUPLICATE_REQUEST_ID = "DRI";
    public static final String REQUESTER_IP_UNAUTHORIZED = "RCIR";
    public static final String RESPONSE_DESC_NOT_AVAILABLE = "N/A";
    public static final String YEAR_MONTH_DAY = "MM/dd/yyyy";
    /**
     * http url connection response
     */
    public static final String HTTP_OK_RESPONSE = "OK";
    public static final String HTTP_CONNECT_TIMEOUT_RESPONSE = "TIMEOUT";
    public static final String HTTP_READ_TIMEOUT_RESPONSE = "READ_TIMEOUT";
    public static final String HTTP_SUSPICIOUS_RESPONSE = "SUSPICIOUS";
    public static final String HTTP_FAILURE_RESPONSE = "FAILURE";
    /**
     * DB exception code constant
     */
    public static final String UNIQUE_CONSTRAINT_VIOLATION_EXP = "DRI";
    public static final String NO_DB_EXP = "SUCC";
    /**
     * txn api called constant
     */
    public static final String TXN_API_CALL_SUCCESS = "Y";
    public static final String TXN_API_CALL_FAILURE = "F";
    public static final String TXN_API_NOT_CALLED = "N";
    public static final String TXN_API_NOT_AVAILABLE = "NA";
    /**
     * Http excetpion
     */
    public static final String CONN_TIMEOUT_EXCEPTION_MSG = "java.net.SocketTimeoutException: connect timed out";
    public static final String READ_TIMEOUT_EXCEPTION_MSG = "java.net.SocketTimeoutException: Read timed out";
    public static final String HTTP_HANDSHAKE_EXCEPTION_MSG = "HTTP transport error: javax.net.ssl.SSLHandshakeException";
    /*
     * Client request system error code
     */
    public static final String EXCEPTION_SYS_ERROR_CODE = "SYSTEM";
    public static final String TIMEOUT_SYS_ERROR_CODE = "TIMEOUT";
    /**
     * Simtv response status
     */
    public static final String SUCCESS_PAYMENT_STATUS = "SUCCESS";

    /*
     * Provider response type
     */
    public static final String PROVIDER_SUCCESS_RESPONSE = "SUCCESS";
    public static final String PROVIDER_TIMEOUT_RESPONSE = "TIMEOUT";
    public static final String PROVIDER_FAILURE_RESPONSE = "FAILURE";

    /*
     * simtv transaction status
     *
     */
    public static final String SIMTV_SERVICE_VENDOR_CODE = "SIMTV_REVISED";

    public static final String SMART_CELL_CLIENT_CODE = "esewa";

    public static interface WalletMode {

        public static final String LOCAL = "LOCAL";
        public static final String ESEWA = "ESEWA";
    }

    public static interface RequestMode {

        public static final String INQUIRY = "INQ";
        public static final String PAYMENT = "PAY";
    }
}
