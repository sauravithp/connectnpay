package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author santosh
 */
@Getter
@Setter
public class LoadClientBalanceRequest extends ModelBase {

    private Long clientAccountId;
    private Long initiatorId;
    private String remarks;
    private Double amount;

}
