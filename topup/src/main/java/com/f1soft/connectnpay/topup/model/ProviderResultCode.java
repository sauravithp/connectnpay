package com.f1soft.connectnpay.topup.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author yogesh
 */
@Entity
@Table(name = "PROVIDER_RESULT_CODE")
@NamedQueries({
    @NamedQuery(name = "ProviderResultCode.findAllActive", query = "SELECT b FROM ProviderResultCode b where b.active = 'Y'")
})
public class ProviderResultCode implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false, precision = 22)
    private Long id;
    @Column(name = "RESULT_CODE", length = 50)
    private String resultCode;
    @Column(name = "RESULT_DESC")
    private String resultDesc;
    @Column(name = "PROVIDER_CODE")
    private String providerCode;
    @Column(name = "RESULT_TYPE")
    private String resultType;
    @Basic(optional = false)
    @Column(name = "ACTIVE")
    private char active;

    public ProviderResultCode() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultDesc() {
        return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public char getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }
}
