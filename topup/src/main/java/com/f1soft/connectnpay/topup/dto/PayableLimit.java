package com.f1soft.connectnpay.topup.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Sauravi Thapa
 * 4/11/21
 */
@Getter
@Setter
public class PayableLimit implements Serializable {
    private Double minAmount;
    private Double maxAmount;
}
