package com.f1soft.connectnpay.topup.builder;

import com.f1soft.connectnpay.topup.dto.BillPayResponse;
import com.f1soft.connectnpay.topup.dto.RequestDTO;
import com.f1soft.connectnpay.topup.dto.ServerResponse;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public class TopupResponseBuilder {
    public static BillPayResponse paymentResponse(ServerResponse serverResponse,
                                                  RequestDTO requestDTO) {
        BillPayResponse billPayResponse = new BillPayResponse();
        billPayResponse.setResultCode(serverResponse.getRespCode());
        billPayResponse.setResultDescription(serverResponse.getMessage());
        billPayResponse.setRequestId(requestDTO.getRequestId());
        billPayResponse.setMobileNumber(requestDTO.getServiceAttribute());
        try {
            billPayResponse.setAmount(String.valueOf(requestDTO.getAmount()));
        } catch (Exception e) {
            billPayResponse.setAmount("");
        }
        //trace id provided by vendor
        if (serverResponse.getVendorTraceId() != null && serverResponse.getVendorTraceId().trim().length() > 0) {
            billPayResponse.setSystemTraceId(serverResponse.getVendorTraceId());
        } else {
            billPayResponse.setSystemTraceId("0");
        }
        billPayResponse.setAgentTransId(requestDTO.getClientTraceId());
        return billPayResponse;
    }

    public static BillPayResponse buildResponse(ServerResponse serverResponse) {
        BillPayResponse billPayResponse = new BillPayResponse();
        billPayResponse.setResultCode(serverResponse.getRespCode());
        billPayResponse.setResultDescription(serverResponse.getMessage());
        return billPayResponse;
    }

    public static BillPayResponse buildResponse(ServerResponse serverResponse, String requestId) {
        BillPayResponse billPayResponse = new BillPayResponse();
        billPayResponse.setResultCode(serverResponse.getRespCode());
        billPayResponse.setResultDescription(serverResponse.getMessage());
        billPayResponse.setRequestId(requestId);
        return billPayResponse;
    }
}
