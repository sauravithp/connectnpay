package com.f1soft.connectnpay.topup.constant;

/**
 * @author Sauravi Thapa
 * 4/8/21
 */
public interface EhCacheConstants {
    String SERVICE = "service";
    String CLIENT = "client";

    public interface HintConstant {
        String NAME = "org.hibernate.cacheable";
    }

    public interface UriConstant {
        String CLEAR_CACHE = "ehcache/clear/cache";
    }
}
